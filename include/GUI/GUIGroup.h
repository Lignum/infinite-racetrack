/* 
 * File:   GUIGroup.h
 * Author: victor
 *
 * Created on 20 de febrero de 2015, 21:22
 */

#ifndef GUIGROUP_H
#define	GUIGROUP_H


#include <OgrePanelOverlayElement.h>
#include <vector>

#include "GUIImage.h"
#include "Cursor.h"
#include "Button.h"
#include "Text.h"
#include "TextField.h"
#include "Slider.h"

using namespace Ogre;

class GUIGroup{
    
    private:
        
        String _name;
        
        Overlay* _overlay;

        std::vector<GUIImage*> _GUIImages;
        std::vector<Button*> _buttons;
        std::vector<TextField*> _textFields;
        std::vector<Text*> _texts;
        std::vector<Slider*> _sliders;
        
        Real _wWidth;
        Real _wHeight;        
        
        Real _charHeight;
        String _textFont;
        ColourValue _textColor;
        
        void setDimensions(unsigned int width, unsigned int height){
            _wWidth = (width > 0) ? width : 1;
            _wHeight = (height > 0) ? height : 1;
        }
        
        bool cursorColision(int cX, int cY, int x, int y, int witdh, int height){
            
            if(cX<x) return false;
            if(cY<y) return false;
            if(cX>x+witdh) return false;
            if(cY>y+height) return false;
            
            return true;
        }
        
    public:
        GUIGroup(std::string name, Real width, Real height){
            _overlay = OverlayManager::getSingletonPtr()->create(name);
            _overlay->setZOrder(640);
            _overlay->show();
            
            _name = name;

            _charHeight = -1;
            _textFont = "";
            _textColor = ColourValue::ZERO;

            
            setDimensions(width, height);
        }
        
        const String getName(){
            return _name;
        }
        
        void firstFrame(){
            for (long unsigned int i=0; i < _texts.size(); i++){
                _texts[i]->update();
            }
        }
        
        void update(Real delta){
            for (long unsigned int i=0; i < _textFields.size(); i++){
                _textFields[i]->update(delta);
            }
        }
        
        void setZOrder(const int z){
            _overlay->setZOrder(z);
        }
        
        int getZOrder(){
            return _overlay->getZOrder();
        }
        
        void setVisible(const bool visible){
            if(visible) {
                _overlay->show();
            } else {
                _overlay->hide();
            }
        }
        
        void setAllvisible(bool visible){
            
            for (long unsigned int i=0; i<_GUIImages.size(); i++){
               _GUIImages[i]->setVisible(visible);
            }
            
            for (long unsigned int i=0; i<_buttons.size(); i++){
               _buttons[i]->setVisible(visible);
            }
            
            for (long unsigned int i=0; i<_texts.size(); i++){
               _texts[i]->setVisible(visible);
            }
                
            for (long unsigned int i=0; i<_textFields.size(); i++){
                _textFields[i]->setVisible(visible);
            }
            
            for (long unsigned int i=0; i<_sliders.size(); i++){
                _sliders[i]->setVisible(visible);
            }
        }
        
        void setDefaultTextCharHeight(Real height){
            
            _charHeight = height;
            
            for (long unsigned int i=0; i < _texts.size(); i++){
                _texts[i]->setCharHeight(height);
            }
        }
        
        void setDefaultTextFont(String font){
            
            _textFont = font;
            
            for (long unsigned int i=0; i < _texts.size(); i++){
                _texts[i]->setFont(_textFont);
            }
        }
        
        void setDefaultTextColor(ColourValue color){
            
            _textColor = color;
            
            for (long unsigned int i=0; i < _texts.size(); i++){
                _texts[i]->setColor(color);
            }
        }
        
        std::vector<GUIImage*> getGUIImages(){                        
            return _GUIImages;
        }
        
        GUIImage* getGUIImage(const String& name){
            for (long unsigned int i=0; i<_GUIImages.size(); i++){
                if( _GUIImages[i]->getName() == _name+"/"+name){
                    return _GUIImages[i];
                }
            }
            
            return NULL;
        }
        
        GUIImage* getGUIImage(const int num){                        
            return _GUIImages[num];
        }
                
        std::vector<Button*> getButtons(){
            return _buttons;
        }
        Button* getButton(const String& name){
            for (long unsigned int i=0; i<_buttons.size(); i++){
                if( _buttons[i]->getName() == _name+"/"+name){
                    return _buttons[i];
                }
            }
            
            return NULL;
        }
        
        Button* getButton(const int num){
            return _buttons[num];
        }
        
        Text* getText(const String& name){
            for (long unsigned int i=0; i<_texts.size(); i++){
                if( _texts[i]->getName() == _name+"/"+name){
                    return _texts[i];
                }
            }
            
            return NULL;
        }
        
        Text* getText(const int num){
            return _texts[num];
        }
        
        std::vector<TextField*> getTextFields(){                        
            return _textFields;
        }
        
        TextField* getTextField(const String& name){
            for (long unsigned int i=0; i<_textFields.size(); i++){
                if( _textFields[i]->getName() == _name+"/"+name){
                    return _textFields[i];
                }
            }
            
            return NULL;
        }
        
        TextField* getTextField(const int num){                        
            return _textFields[num];
        }
        
         std::vector<Slider*> getSliders(){                        
            return _sliders;
        }
        
        Slider* getSlider(const String& name){
            for (long unsigned int i=0; i<_sliders.size(); i++){
                if( _sliders[i]->getName() == name){
                    return _sliders[i];
                }
            }
            
            return NULL;
        }
        
        Slider* getSlider(const int num){                        
            return _sliders[num];
        }
        
        void keyPressed(const OIS::KeyEvent &e){
            for (long unsigned int i=0; i<_textFields.size(); i++){                    
                if(_textFields[i]->isSelected()){                    
                    _textFields[i]->keyPressed((char)e.text);
                }

            }
        }
        
        void moveCursor(int x, int y){
            for (long unsigned int i=0; i<_buttons.size(); i++){

                if(_buttons[i]->isVisible()){
                    if(cursorColision(x,y, 
                            _buttons[i]->getX(), _buttons[i]->getY(),
                            _buttons[i]->getWidth(), _buttons[i]->getHeight())){

                        if(!_buttons[i]->isClick()){                            
                            _buttons[i]->setFocus(true);


                        }
                    }else{
                        _buttons[i]->setFocus(false);
                        _buttons[i]->setClick(false);
                    }
                }

            }

            for (long unsigned int i=0; i<_textFields.size(); i++){

                if(_textFields[i]->isVisible()){
                    if(cursorColision(x,y, 
                            _textFields[i]->getX(), _textFields[i]->getY(),
                            _textFields[i]->getWidth(), _textFields[i]->getHeight())){

                        if(!_textFields[i]->isSelected()){                            
                            _textFields[i]->setFocus(true);


                        }
                    }else{
                        _textFields[i]->setFocus(false);
                    }
                }

            }

            for (long unsigned int i=0; i<_sliders.size(); i++){
                if(_sliders[i]->isVisible()){
                    if(_sliders[i]->isClick()){
                        _sliders[i]->setValPosition(x);
                    }

                    if(cursorColision(x,y, 
                            _sliders[i]->getX(), _sliders[i]->getY(),
                            _sliders[i]->getWidth(), _sliders[i]->getHeight())){

                        if(!_sliders[i]->isClick()){                            
                            _sliders[i]->setFocus(true);
                        }
                    }else{
                        _sliders[i]->setFocus(false);
                    }
                }

            }
        }
        
        void clickPressed(OIS::MouseButtonID id){

            if(id == OIS::MB_Left){
                for (long unsigned int i=0; i<_buttons.size(); i++){
                    if(_buttons[i]->isFocus()){
                        _buttons[i]->setClick(true);
                    }

                }

                for (long unsigned int i=0; i<_textFields.size(); i++){ 

                    _textFields[i]->setSelected(false);

                    if(_textFields[i]->isFocus()){
                        _textFields[i]->setSelected(true);
                    }

                }

                for (long unsigned int i=0; i<_sliders.size(); i++){
                    if(_sliders[i]->isFocus()){
                        _sliders[i]->setClick(true);
                    }
                }
                    
            }
        }
        
        void clickReleased(OIS::MouseButtonID id){
            for (long unsigned int i=0; i<_buttons.size(); i++){
                _buttons[i]->setClick(false);
            }

            for (long unsigned int i=0; i<_sliders.size(); i++){
                _sliders[i]->setClick(false);
            }
        }
        
        int getButtonFocusNum(){
            for (long unsigned int i=0; i<_buttons.size(); i++){
                if(_buttons[i]->isVisible()){
                    if(_buttons[i]->isFocus()){
                        return i;
                    }
                }
            }
            
            return -1;
        }
        
        void unFocus(){
            for (long unsigned int i=0; i<_buttons.size(); i++){
                _buttons[i]->setFocus(false);
            }
            for (long unsigned int i=0; i<_textFields.size(); i++){
                _textFields[i]->setFocus(false);
                _textFields[i]->setSelected(false);
            }
        }
        
        void add(GUIImage* GUIElement){
            _GUIImages.push_back(GUIElement);
            _overlay->add2D(GUIElement->getContainer());
            GUIElement->setDimensions(_wWidth, _wHeight);
        }
        
        void add(Text* text){
            
            _texts.push_back(text);
            _overlay->add2D(text->getContainer());
                        
            text->setDimensions(_wWidth, _wHeight);
            
            if(_textFont != ""){
                text->setFont(_textFont);
            }
            
            if(_charHeight != -1){
                text->setCharHeight(_charHeight);
            }
            
            if(_textColor != ColourValue::ZERO){
                text->setColor(_textColor);
            }
            //GUIElement->setDimensions(_wWidth, _wH)eight);
        }
        
        void add(Button* button){
            _buttons.push_back(button);
            _overlay->add2D(button->getContainer());
            add(button->getText());
            button->setDimensions(_wWidth, _wHeight);
            button->centerText();
        }
        
        void add(TextField* field){
            _textFields.push_back(field);
            _overlay->add2D(field->getContainer());
            
            add(field->getText());
            field->setDimensions(_wWidth, _wHeight);
        }
        
        void add(Slider* slider){
            
            add(slider->getBar());
            _sliders.push_back(slider);
            _overlay->add2D(slider->getContainer());
            add(slider->getText());
            add(slider->getValText());
            slider->setDimensions(_wWidth, _wHeight);
        }
        
        void nextButton(int n){
            
            bool sel = false;
            
            for(int i=0; i< (int)_buttons.size(); i++){
                if(_buttons[i]->isFocus()){
                    sel = true;
                    
                    _buttons[i]->setFocus(false);                    
                                        
                    if(i+n >= (int)_buttons.size()){
                        n = n-1;
                    }else if(i+n < 0){                    
                        n = _buttons.size() + (i+n);
                    }else{
                        n = i+n;
                    }
                    
                    _buttons[n]->setFocus(true);
                    break;
                }
            }
            
            if(!sel){
                _buttons[0]->setFocus(true);
            }
        }
};

#endif	/* GUIGROUP_H */

