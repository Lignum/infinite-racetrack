/* 
 * File:   Button.h
 * Author: victor
 *
 * Created on 11 de diciembre de 2014, 14:03
 */

#ifndef BUTTON_H
#define	BUTTON_H

#include "GUIImage.h"
#include "Text.h"
#include "Console.h"


class Button : public GUIImage{
    
    private:        
        TexturePtr _focusTexture;
        MaterialPtr _focusMaterial;  
        
        TexturePtr _clickTexture;
        MaterialPtr _clickMaterial;
        
        Text* _text;
        
        bool _focus;
        bool _click;

    public:
        
        Button(const String& name, const String& texture, const String& text): GUIImage(name, texture){
            init(name, text, "");
        }
        
        Button(const String& name, const String& texture, const String& focus, const String& click, const String& text): GUIImage(name, texture){
            init(name, text, "");
            
            setFocusImage(focus);
            setClickImage(click);
        }
        
        Button(const String& name, const String& texture, const String& text, const String& font ): GUIImage(name, texture){
            init(name, text, font);
        }
        
        void init(const String& name, const String& text, const String& font){
            _text = new Text(name+"/text", text, font);
            _focus = false;
            _click = false;
        }
    
        ~Button(){
        }
        
        void update(){
            _text->update();
        }
        
        void setPosition(Real x, Real y){
            
            int tX = _text->getX()-getX();
            int tY = _text->getY()-getY();
            
            GUIImage::setPosition(x, y);
            
            _text->setPosition(x+tX, y+tY);
        }
        
        
        void setFocusImage(const String& filename){
            _focusMaterial = MaterialManager::getSingleton().create(_name+"/focus/default", "General");
            _focusTexture = TextureManager::getSingleton().load(filename, "General");

            TextureUnitState *pTexState;
            if(_focusMaterial->getTechnique(0)->getPass(0)->getNumTextureUnitStates()){
                pTexState = _focusMaterial->getTechnique(0)->getPass(0)->getTextureUnitState(0);
            } else {
                pTexState = _focusMaterial->getTechnique(0)->getPass(0)->createTextureUnitState( _focusTexture->getName() );
            }
            pTexState->setTextureAddressingMode(TextureUnitState::TAM_CLAMP);
            _focusMaterial->getTechnique(0)->getPass(0)->setSceneBlending(SBT_TRANSPARENT_ALPHA); 
        }
        
        void setClickImage(const String& filename){
            _clickMaterial = MaterialManager::getSingleton().create(_name+"/click/default", "General");
            _clickTexture = TextureManager::getSingleton().load(filename, "General");

            
            TextureUnitState *pTexState;
            
            if(_clickMaterial->getTechnique(0)->getPass(0)->getNumTextureUnitStates()){
                pTexState = _clickMaterial->getTechnique(0)->getPass(0)->getTextureUnitState(0);
            } else {
                pTexState = _clickMaterial->getTechnique(0)->getPass(0)->createTextureUnitState( _clickTexture->getName() );
            }
            pTexState->setTextureAddressingMode(TextureUnitState::TAM_CLAMP);
            _clickMaterial->getTechnique(0)->getPass(0)->setSceneBlending(SBT_TRANSPARENT_ALPHA); 
        }

        void centerText(){
            
            _text->getContainer()->setPosition(getX(), getY());
            //_text->getContainer()->setDimensions(_texture->getWidth() / _wWidth, _texture->getHeight() / _wHeight);
            
            //_text->setPosition(_texture->getWidth() /2, (_texture->getHeight()/2) - (_text->getHeight()/2 * 6));
            _text->setPosition(_texture->getWidth() /2, (getHeight()/2) - (_text->getHeight()/2));
            _text->setAlignment(TextAreaOverlayElement::Center);
            
        }
        
        void setTextPosition(Real x, Real y){
            _text->setPosition(getX()+x, getY()+y);
        }
        
        void setWidth(const int width){
            Element::setWidth(width);
            _texture.getPointer()->setWidth(width);
        }
        
        Text* getText(){
            return _text;
        }
        
        void setFocus(bool focus){
            _focus = focus;
            if(!_focusMaterial.isNull()){
                if(focus){
                    _container->setMaterialName(_focusMaterial->getName());
                }else{
                    _container->setMaterialName(_material->getName());
                }
            }
            
        }
        
        void setClick(bool click){
            _click = click;
            if(!_clickMaterial.isNull()){
                if(click){
                    _container->setMaterialName(_clickMaterial->getName());
                }else{
                    _container->setMaterialName(_material->getName());
                }
            }
        }
        
        bool isFocus(){
            return _focus;
        }

        bool isClick(){
            return _click;
        }
       
        
        template <class T>
        void click (void(T::*pmemfn)(), T& p){            
            if(_click ) (p.*pmemfn)();            
        }
        
        template <class T>
        void enter (void(T::*pmemfn)(), T& p){
            if(_focus ) {
                setClick(true);
                (p.*pmemfn)();
            }
            
        }
        
        void setVisible(bool visible){
            if(visible) {
                _container->show();
            } else {
                _container->hide();
                _click = false;
                _focus = false;
            }
            _text->setVisible(visible);
        }
};

#endif	/* BUTTON_H */

