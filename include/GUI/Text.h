/* 
 * File:   Text.h
 * Author: victor
 *
 * Created on 11 de diciembre de 2014, 15:31
 */

#ifndef TEXT_H
#define	TEXT_H

#include "Element.h"
//#include <OgrePanelOverlayElement.h>
#include "OgreTextAreaOverlayElement.h"
 
using namespace Ogre;

class Text : public Element{
    
    private:
        
        TextAreaOverlayElement* _textArea;
    public:
        
        Text(const String& name, const String& text): Element(name){
            init(name, text, "");
        }
        
        Text(const String& name, const String& text, const String& font): Element(name){
            
            init(name, text, font);

           
        }
        
        void init(const String& name, const String& text, const String& font){
            _container->setMetricsMode(Ogre::GMM_PIXELS);
            _container->setDimensions(1.0f,1.0f);
            
            _textArea=static_cast<TextAreaOverlayElement*>(OverlayManager::getSingletonPtr()->createOverlayElement("TextArea","szElement/"+name));
            _container->addChild(_textArea);
            
            if(font != ""){
                _textArea->setFontName(font);
            }
            _textArea->setMetricsMode(Ogre::GMM_RELATIVE);
            _textArea->setCaption(text);
        }
        
        ~Text(){}
        
        void update(){
            setText(_textArea->getCaption());
            //_textArea->setCaption(_textArea->getCaption());
        }
        
        TextAreaOverlayElement* getTextArea(){
            return _textArea;
        }
        
        
        void setText(String szString){
            _textArea->setCaption(szString);
            //textArea->setDimensions(1.0f,1.0f);
            //textArea->setFontName("Blue");
            //_textArea->setCharHeight(0.03f);
            //_textArea->show();
            
            //textArea->setVerticalAlignment(Ogre::GVA_CENTER);
            
        }
        
        void setCharHeight(Real height){
            _textArea->setCharHeight(height / _wHeight * 600);
        }
        
        void setColor(const Ogre::ColourValue color){
            
            _textArea->setColour(color);
        }
        
        void setFont(String font){
            _textArea->setFontName(font);
        }
        
        void setPosition(Real x, Real y){
            //Element::setPosition(x, y);
            Real rx = x / _wWidth;
            Real ry = y / _wHeight;
            //_container->setPosition(x,y);
            _textArea->setPosition(rx, ry);
            
        }
        
        void setAlignment(Ogre::TextAreaOverlayElement::Alignment a){
            _textArea->setAlignment(a);
        }
        
        /*OverlayContainer* getContainer(){
            return _container;
        }*/
        
        void setDimensions(unsigned int width, unsigned int height){
            _wWidth = (width > 0) ? width : 1;
            _wHeight = (height > 0) ? height : 1;

            
            //textArea->setDimensions(_wWidth, _wHeight);
            //_container->setWidth(_wWidth);
            //_container->setHeight(_wHeight);
            
            //_textArea->setWidth(_wWidth);
            //_textArea->setHeight(_wHeight);
        }
        
        
        Real getX(){
            
            Real x = _textArea->_getLeft() * _wWidth;
            return x;
        }
        
        Real getY(){
            
            Real y = _textArea->_getTop() * _wHeight;
            return y;
        }
        
        
        int getWidth(){
            
            int width = _textArea->getCaption().size() * 0.0115 * _wHeight;
            //std::cout << "with " << width << std::endl;  
            return width;
        }
        
        float getHeight(){
            
                       
            float height = _textArea->getCharHeight() * _wHeight;
            return height;
        }
        
        /*const String getName(){
            return _name;
        }*/
        
        const String getText(){
            return _textArea->getCaption();
        }
        
        ColourValue getColor(){
            return _textArea->getColour();
        }
    
};

#endif	/* TEXT_H */

