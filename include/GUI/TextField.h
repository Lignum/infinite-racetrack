/* 
 * File:   TextField.h
 * Author: victor
 *
 * Created on 20 de enero de 2015, 16:34
 */

#ifndef TEXTFIELD_H
#define	TEXTFIELD_H

#include "GUIImage.h"
#include "Text.h"
#include "Console.h"

class TextField: public GUIImage{
    
    private:
        Text* _text;
        
        bool _selected;
        bool _focus;
        
        bool _upper;
        int _maxChar;
        
        bool _lastC;
        int _countC;
    
    public:
        TextField(const String& name, const String& texture, const String& text): GUIImage(name, texture){
            init(name, text, "");
        }
        
        void init(const String& name, const String& text, const String& font){
            _text = new Text(name+"/text", text, font);
            //_text->setPosition(0,0);
            _focus = false;
            _selected = false;
            
            _upper = false;
            _maxChar = 50;
            
            _lastC = false;
            _countC = 100;
            
        }
        
        void update(Real delta){
           
            /*if(_selected){

                if(_countC <= 0){
                    if(_lastC){
                        _text->setText(_text->getText()+"_");
                    }else{
                        if(_text->getText().length()>0){
                            String s = _text->getText();  
                            s.erase(s.length()-1,1);
                            _text->setText(s);
                        }
                    }
                    _lastC = !_lastC;
                    _countC = 50;
                }

                _countC -= 1*delta;
                
            }else{
                if(!_lastC){                       
                    if(_text->getText().length()>0){
                        String s = _text->getText();  
                        s.erase(s.length()-1,1);
                        _text->setText(s);
                    }
                    _lastC = true;
                }
            }*/
        }
        
        void setVisible(bool visible){
            if(visible) {
                _container->show();
            } else {
                _container->hide();
            }
            _text->setVisible(visible);
        }
        
        void setFocus(bool focus){
            _focus = focus;
        }
        
        bool isFocus(){
            return _focus;
        }
        
        void setSelected(bool selected){
            _selected = selected;
                    
        }
        
        bool isSelected(){
            return _selected;
        }
        
        void setOnlyUpper(bool upper){
            _upper = upper;
        }
        
        void setMaxChar(int max){
            _maxChar = max;
        }
        
        void keyPressed(char c){
                        
            if(c == 8){//Retroceso
                
                if(_text->getText().length()>0){
                    String s = _text->getText();  
                    s.erase(s.length()-1,1);
                    _text->setText(s);
                    
                    if(!_lastC && _text->getText().length()>0){
                        String s = _text->getText();  
                        s.erase(s.length()-1,1);
                        _text->setText(s);
                    }
                    
                    _lastC = true;
                }
                
            }else{
                if((int)(_text->getText().size())<_maxChar){
                    if(c>=32 && c<=126){
                        if(_upper){
                            c = putchar(toupper(c));
                        }
                        if(!_lastC){
                            String s = _text->getText();
                            s.erase(s.length()-1,1);
                            _text->setText(s);
                            _lastC = true;
                        }
                        
                        char* ch = &c;
                        _text->setText(_text->getText()+ch[0]);
                    }
                }
            }
            
            
        }
        
        void setPosition(Real x, Real y){
            
            int tX = _text->getX()-getX();
            int tY = _text->getY()-getY();
            
            GUIImage::setPosition(x, y);
            
            _text->setPosition(x+tX, y+tY);
        }
        
        Text* getText(){
            return _text;
        }
        void centerText(){
            
            _text->getContainer()->setPosition(getX(), getY());
            _text->getContainer()->setDimensions(_texture->getWidth() / _wWidth, _texture->getHeight() / _wHeight);
            
            _text->setPosition(_texture->getWidth() /2, (_texture->getHeight()/2) -_text->getHeight()/2);
            _text->setAlignment(TextAreaOverlayElement::Center);
            
        }
        
        void setTextPosition(Real x, Real y){
            _text->setPosition(getX()+x, getY()+y);
        }
    
};


#endif	/* TEXTFIELD_H */

