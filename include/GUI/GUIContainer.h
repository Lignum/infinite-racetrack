/* 
 * File:   GUIContainer.h
 * Author: victor
 *
 * Created on 10 de diciembre de 2014, 23:18
 */

#ifndef GUICONTAINER_H
#define	GUICONTAINER_H

#include <OgrePanelOverlayElement.h>
#include <vector>

#include "GUIImage.h"
#include "Cursor.h"
#include "Button.h"
#include "Text.h"
#include "TextField.h"
#include "Slider.h"
#include "GUIGroup.h"
 
using namespace Ogre;

class GUIContainer{
    
    private:
        
        static Overlay* _overlay;
        static Overlay* _cursorOverlay;
        static Cursor* _cursor;
        
        static std::vector<GUIGroup*> _groups;
        
        static bool _enable;
        static bool _firstFrame;
        
        static Real _wWidth;
        static Real _wHeight;        
        
        static Real _charHeight;
        static String _textFont;
        static ColourValue _textColor;
        
        
        static void setDimensions(unsigned int width, unsigned int height){
            _wWidth = (width > 0) ? width : 1;
            _wHeight = (height > 0) ? height : 1;
        }
        
        static bool cursorColision(int x, int y, int witdh, int height){
            
            if(_cursor->getX()<x) return false;
            if(_cursor->getY()<y) return false;
            if(_cursor->getX()>x+witdh) return false;
            if(_cursor->getY()>y+height) return false;
            
            return true;
        }

        
    public:
        static void init(Viewport* viewport){
            
            /*_overlay = OverlayManager::getSingletonPtr()->create("GUI");
            _overlay->setZOrder(600);
            _overlay->show();*/
            
            _cursorOverlay = OverlayManager::getSingletonPtr()->create("CURSOROV");
            _cursorOverlay->setZOrder(649);
            _cursorOverlay->show();
            
            _cursor = NULL;
            
            _charHeight = -1;
            _textFont = "";
            _textColor = ColourValue::ZERO;
            
            _enable = true;
            _firstFrame = true;
            setDimensions(viewport->getActualWidth(), viewport->getActualHeight());
            
            addGroup(new GUIGroup("DEFAULT", _wWidth, _wHeight));
        }
        
       

        ~GUIContainer(void) {}
        
        static void update(Real delta){
            
            if(_firstFrame){
                for (long unsigned int i=0; i < _groups.size(); i++){           
                    _groups[i]->firstFrame();                    
                }
                _firstFrame = false;
            }
            
            for (long unsigned int i=0; i < _groups.size(); i++){           
                _groups[i]->update(delta);                    
            }

        }
        
        static void setZOrder(int z){
            _groups[0]->setZOrder(z);
        }
        
        static void setVisible(bool visible){
            _groups[0]->setVisible(visible);
            
        }
        
        static void setAllvisible(bool visible){
            
            for (long unsigned int i=0; i<_groups.size(); i++){
                _groups[i]->setAllvisible(visible);
            }
        }
        
        static void unFocus(){
            
            for(unsigned int i=0; i<_groups.size(); i++){
                _groups[i]->unFocus();
            }
            
        }
        
        static void showCursor(bool show){
            _cursor->setVisible(show);
        }
        static bool cursorIsVisible(){
            return _cursor->isVisible();
        }
                
        
        static void createCursor(const String& texture){
            if(OverlayManager::getSingletonPtr()->hasOverlayElement("Element/cursor")){
               showCursor(true);
            }else{
                addCursor(new Cursor("cursor", texture));
            }
        }
        
        static void createImage(const String& name, const String& texture){
            createImage("DEFAULT", name, texture);
        }
        
        static void createImage(const String& group, const String& name, const String& texture){
            if(OverlayManager::getSingletonPtr()->hasOverlayElement("Element/"+group+"/"+name)){
                getGUIImage(group, name)->setVisible(true);
            }else{
                getGroup(group)->add(new GUIImage(group+"/"+name, texture));
            }
        }
        
        static void createButton(const String& name, const String& texture, const String& focus, const String& click, const String& text){
            
            createButton("DEFAULT", name, texture, focus, click, text);
        }
        
        static void createButton(const String& group, const String& name, const String& texture, const String& focus, const String& click, const String& text){
            if(OverlayManager::getSingletonPtr()->hasOverlayElement("Element/"+group+"/"+name)){
                getButton(group, name)->setVisible(true);
            }else{
                getGroup(group)->add(new Button(group+"/"+name, texture, focus, click, text));
            }
        }
        
        static void createText(const String& name, const String& text){
            createText("DEFAULT", name, text);
        }
        
        static void createText(const String& group, const String& name, const String& text){
            if(OverlayManager::getSingletonPtr()->hasOverlayElement("Element/"+group+"/"+name)){
                getText(group, name)->setVisible(true);
            }else{
                getGroup(group)->add(new Text(group+"/"+name, text));
                _firstFrame = true;
            }
        }
        
        static void createTextField(const String& name, const String& texture, const String& text){
            createTextField("DEFAULT", name, texture, text);
        }
        
        static void createTextField(const String& group, const String& name, const String& texture, const String& text){
            if(OverlayManager::getSingletonPtr()->hasOverlayElement("Element/"+group+"/"+name)){
                getTextField(group, name)->setVisible(true);
            }else{
                getGroup(group)->add(new TextField(group+"/"+name, texture, text));
                _firstFrame = true;
            }
        }
        
        static void createSlider(const String& name, const String& texture, const String& focus, const String& click, const String& bar, const String& text){
            
            std::cout << "44444444444444444444444444444444444444444" << std::endl;
            createSlider("DEFAULT", name, texture, focus, click, bar, text);
        }
        
        static void createSlider(const String& group, const String& name, const String& texture, const String& focus, const String& click, const String& bar, const String& text){
            
            std::cout << "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" << std::endl;
            if(OverlayManager::getSingletonPtr()->hasOverlayElement("Element/"+group+"/"+name)){
                getSlider(group, name)->setVisible(true);
            }else{
            std::cout << "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" << std::endl;
                getGroup(group)->add(new Slider(group+"/"+name, texture, focus, click, bar, text));
            }
        }
        
        static void createGroup(const String& name){
            if(!OverlayManager::getSingletonPtr()->getByName(name)){
                addGroup(new GUIGroup(name, _wWidth, _wHeight));
            }
        }
        
        
        static void addGroup(GUIGroup* group){
            _groups.push_back(group);
            
            group->setDefaultTextCharHeight(_charHeight);
            group->setDefaultTextColor(_textColor);
            group->setDefaultTextFont(_textFont);
        }
        
        static void addCursor(Cursor* cursor){
            
            _cursor = cursor;
            _cursorOverlay->add2D(cursor->getContainer()); 
            _cursor->setDimensions(_wWidth, _wHeight);
            
        }
        
        static void setEnable(bool enable){
            _enable = enable;
        }
        
        static void moveCursor(int x, int y){
            
            if(_cursor){
                _cursor->setPosition(x ,y);
                
                for(unsigned int i=0; i<_groups.size(); i++){
                    _groups[i]->moveCursor(x,y);
                }
                
            } else {
                std::cerr << "Error: No cursor found" << std::endl;
            }
        }
        
        static void clickPressed(OIS::MouseButtonID id){
            moveCursor(_cursor->getX(), _cursor->getY());
            
            
            if(_cursor){
                if(_cursor->isVisible()){                    
                    
                    for (long unsigned int i=0; i < _groups.size(); i++){                    
                        _groups[i]->clickPressed(id);                  
                    }
                }
            }
        }
        
        static void clickReleased(OIS::MouseButtonID id){
            for (long unsigned int i=0; i<_groups.size(); i++){
                _groups[i]->clickReleased(id);
            }
        }
                
        static void keyPressed(const OIS::KeyEvent &e){
            
            for (long unsigned int i=0; i<_groups.size(); i++){
                _groups[i]->keyPressed(e);
            }
        }
        
        static void setDefaultTextCharHeight(Real height){
            
            _charHeight = height;
            
            for (long unsigned int i=0; i < _groups.size(); i++){
                _groups[i]->setDefaultTextCharHeight(height);
            }
        }
        
        static void setDefaultTextFont(String font){
            
            _textFont = font;
            
            for (long unsigned int i=0; i < _groups.size(); i++){
                _groups[i]->setDefaultTextFont(font);
            }
        }
        
        static void setDefaultTextColor(ColourValue color){
            
            _textColor = color;
            
            for (long unsigned int i=0; i < _groups.size(); i++){
                _groups[i]->setDefaultTextColor(color);
            }
        }
        
        static std::vector<GUIImage*> getGUIImages(){
            return _groups[0]->getGUIImages();
        }
        
        static GUIImage* getGUIImage(const String& name){            
            return _groups[0]->getGUIImage(name);
        }
        
        static GUIImage* getGUIImage(const String& group, const String& name){            
            return getGroup(group)->getGUIImage(name);
        }
        
        static GUIImage* getGUIImage(const int num){                        
            return _groups[0]->getGUIImage(num);
        }
        
        static std::vector<Button*> getButtons(){
            return _groups[0]->getButtons();
        }
        
        static Button* getButton(const String& name){ 
            return _groups[0]->getButton(name);
        }
       
        static Button* getButton(const String& group, const String& name){
            return getGroup(group)->getButton(name);
        }
        
        static Button* getButton(const int num){                        
            return _groups[0]->getButton(num);
        }
        
        static Text* getText(const String& name){                        
            return _groups[0]->getText(name);
        }
        
        static Text* getText(const String& group, const String& name){
            return getGroup(group)->getText(name);
        }
        
        static Text* getText(const int num){                        
            return _groups[0]->getText(num);
        }
        
        static std::vector<TextField*> getTextFields(){
            return _groups[0]->getTextFields();
        }
        
        static TextField* getTextField(const String& name){            
            return _groups[0]->getTextField(name);
        }
        
        static TextField* getTextField(const String&  group, const String& name){            
            return getGroup(group)->getTextField(name);
        }
        
        static TextField* getTextField(const int num){                        
            return _groups[0]->getTextField(num);
        }
        
        static std::vector<Slider*> getSliders(){                        
            return _groups[0]->getSliders();
        }
        
        static Slider* getSlider(const String& name){
            return _groups[0]->getSlider(name);
        }
        
        static Slider* getSlider(const String& group, const String& name){
            return getGroup(group)->getSlider(name);
        }
        
        static Slider* getSlider(const int num){                        
            return _groups[0]->getSlider(num);
        }
        
        static GUIGroup* getGroup(const String& name){
            for (long unsigned int i=0; i<_groups.size(); i++){
                if( _groups[i]->getName() == name){
                    return _groups[i];
                }
            }
            
            return NULL;
        }
        
        static GUIGroup* getGroup(const int num){
                        
            return _groups[num];
        }

};

#endif	/* GUICONTAINER_H */

