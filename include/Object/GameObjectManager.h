/* 
 * File:   GameObjectManager.h
 * Author: victor
 *
 * Created on 28 de enero de 2015, 19:34
 */

#ifndef GAMEOBJECTMANAGER_H
#define	GAMEOBJECTMANAGER_H

#include <Ogre.h>

#include "GameObject.h"
#include "PhysicGameObject.h"
#include "Script.h"


#include <OgreBulletDynamicsRigidBody.h>
#include <Shapes/OgreBulletCollisionsStaticPlaneShape.h>
#include <Shapes/OgreBulletCollisionsBoxShape.h>

#include "Shapes/OgreBulletCollisionsConvexHullShape.h"
#include "Shapes/OgreBulletCollisionsTrimeshShape.h"		
#include "Utils/OgreBulletCollisionsMeshToShapeConverter.h"
#include "OgreBulletCollisionsRay.h"

using namespace Ogre;

class GameObjectManager{
    
    SceneManager* _sceneMgr;
    std::vector<GameObject*> _gameObjects;
    std::vector<Script*> _scripts;
    
    
    OgreBulletDynamics::DynamicsWorld *_world;
    void initPhysics();
    
public:
    GameObjectManager(SceneManager* sce);
    
    void addObject(GameObject* object);
    void addScript(Script* scri);
    
    GameObject* getObjectByName(const String& name);
    GameObject* getObject(const int num);    
    
    PhysicGameObject* getPhysicObjectByName(const String& name);
    PhysicGameObject* getPhysicObject(const int num);
        
    void clear();
    void destroyObject(const String& name);  
    void destroyObject(int i);    
    void destroyObject(int start, int end);
    void setVisible(bool visible);
    
    std::vector<GameObject*> getObjects();
    
    OgreBulletDynamics::DynamicsWorld* getWorld();
    
    void update(Real delta);
    void keyPressed(const OIS::KeyEvent &e);
    void keyReleased(const OIS::KeyEvent &e);    

    void buttonPressed( const OIS::JoyStickEvent &e, int button );
    void buttonReleased( const OIS::JoyStickEvent &e, int button );
    void axisMoved( Real rel, int axis ); 
    void povMoved( const OIS::JoyStickEvent &e, int index);
    
    SceneManager* getSceneManager();
    
};


#endif	/* GAMEOBJECTMANAGER_H */

