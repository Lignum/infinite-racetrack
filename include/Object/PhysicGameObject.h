/* 
 * File:   PhysicGameObject.h
 * Author: victor
 *
 * Created on 17 de marzo de 2015, 17:10
 */

#ifndef PHYSICGAMEOBJECT_H
#define	PHYSICGAMEOBJECT_H

#include "GameObject.h"


#include <OgreBulletDynamicsRigidBody.h>
#include <Shapes/OgreBulletCollisionsStaticPlaneShape.h>
#include <Shapes/OgreBulletCollisionsBoxShape.h>

#include "Shapes/OgreBulletCollisionsConvexHullShape.h"
#include "Shapes/OgreBulletCollisionsTrimeshShape.h"		
#include "Utils/OgreBulletCollisionsMeshToShapeConverter.h"
#include "OgreBulletCollisionsRay.h"

using namespace Ogre;

class PhysicGameObject : public GameObject{
    
    private:        
        
        void init(OgreBulletDynamics::DynamicsWorld* world, bool stati, const String& mesh, Vector3 pos, Vector3 rot, Real bouncines, Real friccion, Real mass);        

        
        Real _bouncines, _friccion, _mass;
        int _shapeType;
        
        Vector3 _shapePos;
        Vector3 _shapeSize;
        
    protected:
        OgreBulletDynamics::RigidBody *_rigidBody;
        
        OgreBulletDynamics::DynamicsWorld* _world;
        bool _static;
        String _mesh;
        
        void initPhysics();
        
    public:
        
        const static int SHAPE_TYPE_MESH = 0;
        const static int SHAPE_TYPE_CUBE = 1;
        
        PhysicGameObject(GameObjectManager* gom, bool stati, const String& mesh, Vector3 pos);
        PhysicGameObject(GameObjectManager* gom, bool stati, const String& mesh, Vector3 pos, Vector3 rot);
        PhysicGameObject(GameObjectManager* gom, bool stati, const String& mesh, Vector3 pos, Vector3 rot, 
            Real bouncines, Real friccion, Real mass);
        PhysicGameObject(GameObjectManager* gom, bool stati, const String& mesh, Vector3 pos, Vector3 rot, 
            Real bouncines, Real friccion, Real mass, int type, Vector3 shapeSize, Vector3 shapePos);
        PhysicGameObject(GameObjectManager* gom, bool stati, const String& name, const String& mesh, Vector3 pos);
        
        virtual ~PhysicGameObject();
        
        virtual void update(Real delta);
                
        virtual void setPosition(Vector3 vec);
        virtual void translate(Real x, Real y, Real z);
        virtual void translate(Vector3 vec);
        virtual void rotX(Real degree);
        virtual void rotY(Real degree);
        virtual void rotZ(Real degree);
        
        virtual void lookAt(Vector3 pos);
        
        bool isCollision(PhysicGameObject* object);
        PhysicGameObject* collision(GameObjectManager* gom);
        
        OgreBulletDynamics::RigidBody* getRigidBody();
        
};

#endif	/* PHYSICGAMEOBJECT_H */

