/* 
 * File:   CheckPoint.h
 * Author: victor
 *
 * Created on 24 de junio de 2015, 15:42
 */

#ifndef CHECKPOINT_H
#define	CHECKPOINT_H



#include "Object/PhysicGameObject.h"
#include "CarControler.h"


#include "Sound/SoundFXManager.h"

using namespace Ogre; 

class CheckPoint : public PhysicGameObject{ 
    
    private:
        const static int _initTime = 100;
        const static float _plusTime = 55;
        
        static CarControler* _car;
        
        static float TIME;
        
        
        bool _check;
        static bool _timeLowStart;
        
        SoundFXPtr _sound;
        static SoundFXPtr _timeLow;
        
        
        static void startSound();
    
    public:
        CheckPoint(GameObjectManager* gom, Vector3 pos, float rot);
        
        void update(Real delta);
        
        static void initTime();
        static void initCar(CarControler* car);
        static void timer(Real delta);
        
        static bool timeOut();
};

#endif	/* CHECKPOINT_H */

