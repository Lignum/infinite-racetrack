/* 
 * File:   Road.h
 * Author: victor
 *
 * Created on 11 de mayo de 2015, 18:24
 */

#ifndef ROAD_H
#define	ROAD_H


#include "Object/PhysicGameObject.h"

using namespace Ogre; 

class Road : public PhysicGameObject{
    
    private:
        int _dir;
        Real _size;
        
        Vector3 _center;
        void calculateCenter();
    
    public:
        
        const static Real DISTANCE = 40;
        
        const static int DIR_N = 0;
        const static int DIR_E = 1;
        const static int DIR_S = 2;
        const static int DIR_W = 3;
        
        
        const static String MESH_S1;
        const static String MESH_R1;
        const static String MESH_L1;
        const static String MESH_R2;
        const static String MESH_L2;
        const static String MESH_R3;
        const static String MESH_L3;
        
        Road(GameObjectManager* gom, const String& mesh, Vector3 pos, int dir);
        Road(GameObjectManager* gom, const String& mesh, Road* last, bool check, int upDown);
        
        int getDirection();
        Vector3 next(int dir);
        
        Real getSize();
        Vector3 getCenter();
};

#endif	/* ROAD_H */

