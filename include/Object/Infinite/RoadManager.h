/* 
 * File:   RoadManager.h
 * Author: victor
 *
 * Created on 11 de mayo de 2015, 18:34
 */

#ifndef ROADMANAGER_H
#define	ROADMANAGER_H

#include "Road.h"


#include "Object/GameObjectManager.h"
#include "Floor.h"
#include "CarControler.h"
#include "Car.h"

using namespace Ogre;

class RoadManager{
    
    private:
        
        Vector3 _position;
        
        std::vector<Road*> _roads;
        
        CarControler* _car;
        
        GameObjectManager* _gom;
        
        float _km;
        int _lastRoadKm;
        
        //Generador 
        const static int _maxRoads = 50;
        const static int _roadsAmount = 7;
        const static int _roadsDistance = 800;
        
        std::vector<float> _roadsTypeRand;
        std::vector<float> _roadsTypeAument;
        
        int _left;
        
        Road* _lastR;
        int _lastRoad;
        
        int _roadsNum;
        int _totalRoads;
        
        int _upDown;
        
        //FLOORS
        std::vector<Floor*> _floors;
        int _floorNum;
        
        //CARS
        int _nextRoadCar;
        static int _maxCar;
        const static int _carCreateDistanceMax = 700;
        const static int _carCreateDistanceMin = 300;
        
        //checkPoint
        const static float _kmPiece = 0.2;
        const static float _kmTotalCheck = 20;
        float _kmCheck;
        
        void createFirst();
        void createRoad();
        void createFloors();
        void calculateKm();
        
        void createCar();
        
        
    
    public:
        RoadManager(GameObjectManager* gom);
        
        void reset();
        
        void createNext();
        
        int getRoadsNum();
        CarControler* getCar();
        void addMaxCars(int num);
        
        static int getMaxRoads();
        
        float getKms();
};

#endif	/* ROADMANAGER_H */

