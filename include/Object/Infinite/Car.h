/* 
 * File:   Car.h
 * Author: victor
 *
 * Created on 14 de mayo de 2015, 17:33
 */

#ifndef CAR_H
#define	CAR_H


#include "Object/PhysicGameObject.h"
#include "Object/Infinite/Road.h"
#include "CarControler.h"

using namespace Ogre;
using namespace OgreBulletCollisions;
using namespace OgreBulletDynamics;

class Car : public PhysicGameObject{
    
    private:
        
        Vector3 _dir;
        float _speed;
        float _lane;
        
        float _speedCollision;
        
        Road* _lastObject;
        
        int _nextCol;
        
        bool _turn;
        int _upDown;
        float _radio;
        float _initZ;
        
        GameObjectManager* _gom;
        
        static std::vector<Road*>* _roads;
        static CarControler* _car;
        
        static const int _maxDistance = 100;
        
        
        void turn();
        void ramp(int dir);
        void straight();
        void rePos();
        void collision();
    
    public:
        
        static int _numCars;
        
        
        Car(GameObjectManager* gom, Vector3 pos, Vector2 dir, int firstRoad, float speed, float lane);
        void update(Real delta);
        
        ~Car();
        
        
        static void setRoads(std::vector<Road*>* roads);
        static void setCarControler(CarControler* car);
        
        static int getNumCars();

};

#endif	/* CAR_H */

