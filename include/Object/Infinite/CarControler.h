/* 
 * File:   CarControler.h
 * Author: victor
 *
 * Created on 20 de mayo de 2015, 14:56
 */

#ifndef CARCONTROLER_H
#define	CARCONTROLER_H

#include <Ogre.h>

#include <OgreBulletDynamicsRigidBody.h>
#include <Shapes/OgreBulletCollisionsStaticPlaneShape.h>
#include <Shapes/OgreBulletCollisionsBoxShape.h>


#include "Shapes/OgreBulletCollisionsBoxShape.h"
#include "Shapes/OgreBulletCollisionsCompoundShape.h"

#include "OgreBulletDynamicsWorld.h"
#include "OgreBulletDynamicsRigidBody.h"
#include "Debug/OgreBulletCollisionsDebugDrawer.h"

#include "Constraints/OgreBulletDynamicsRaycastVehicle.h"


#include "Sound/SoundFXManager.h"

#include "../GameObjectManager.h"

using namespace OgreBulletCollisions;
using namespace OgreBulletDynamics;

class CarControler : public GameObject{
    
    private:
        
        GameObjectManager* _gom;
        
        OgreBulletDynamics::WheeledRigidBody  *_CarChassis;
        OgreBulletDynamics::VehicleTuning     *_Tuning;
        OgreBulletDynamics::VehicleRayCaster  *_VehicleRayCaster;
        OgreBulletDynamics::RaycastVehicle    *_Vehicle;

        Ogre::Entity    *_Chassis;
        Ogre::Entity    *_Wheels[4];
        Ogre::SceneNode *_WheelNodes[4];


        static const float gWheelRadius = 0.285f;
        static const float gWheelWidth = 0.2f;
        static const float gWheelFriction = 1e30f;
        static const float gRollInfluence = 0.1f;
        static const float gSuspensionRestLength = 0.8;
        static const float gEngineForce = 1000.0;
        
        
        static const float _maxSteer = 0.8f;
        static const float _maxDriftDir = 0.815f;
        
        bool _butThrottle;
        bool _butBreak;
        
        float _dir;
        bool _drift, _drifting;
        Real _driftDir;
        
        Real _steer;
        
        Real _acceleration;
        Real _maxSpeed;
        Real _speed;
        
        float _kmh;
        
        //SOUND
        const static float _maxVolume = 0.1;
        SoundFXPtr _Smotor;
        SoundFXPtr _Sdrift;
        
        
        void aceleration(Real delta);
        void steer(Real delta);
        void drift(Real delta);
        

    public:
        CarControler(GameObjectManager* gom, Vector3 pos);
        
        void destroy();
        
        void update(Real delta);
        void keyPressed(const OIS::KeyEvent &e);
        void keyReleased(const OIS::KeyEvent &e);

        void buttonPressed( const OIS::JoyStickEvent &e, int button );
        void buttonReleased( const OIS::JoyStickEvent &e, int button );
        void axisMoved( Real rel, int axis ); 
        void povMoved( const OIS::JoyStickEvent &e, int index);
        
        Real getSteer();
        float getKmh();
        
        void collide();
        void silence();
        
        Vector3 getPosition();
        Vector3 getLinearVelocity();
        
};

#endif	/* CARCONTROLER_H */

