/* 
 * File:   Wall.h
 * Author: victor
 *
 * Created on 1 de julio de 2015, 18:03
 */

#ifndef WALL_H
#define	WALL_H


#include "Object/GameObject.h"
#include "Object/PhysicGameObject.h"

using namespace Ogre;

class Wall : public GameObject{
    
    private:
        
        GameObjectManager* _gom;
        
        PhysicGameObject* _col1;
        PhysicGameObject* _col2;
    
    public:
        Wall(GameObjectManager* gom, const String& mesh, int type, Vector3 pos, int rot);
        
        ~Wall();
};
#endif	/* WALL_H */

