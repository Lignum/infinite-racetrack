/* 
 * File:   Floor.h
 * Author: victor
 *
 * Created on 1 de junio de 2015, 18:44
 */

#ifndef FLOOR_H
#define	FLOOR_H


#include "../GameObjectManager.h"


class Floor : public PhysicGameObject{
    public:
        
        const static float SIZE = 800;
        const static float HEIGHT = 0.15;
        
        Floor(GameObjectManager* gom, Vector3 pos);
        Vector3 getRelativePosition();
        

};

#endif	/* FLOOR_H */

