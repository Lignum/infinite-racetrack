/* 
 * File:   CameraControl.h
 * Author: victor
 *
 * Created on 28 de mayo de 2015, 15:25
 */

#ifndef CAMERACONTROL_H
#define	CAMERACONTROL_H

#include "CarControler.h"

class CameraControl{
    private:
        Camera* _camera;
        CarControler* _car;
        
        const static float DISTANCE = 5;
        const static float HEIGHT = 1.5;
        
                
        void translateCamera(Real delta);
    
    public:
        CameraControl(GameObjectManager* gom, Camera* camera, CarControler* car);
        
        void update(Real delta);
};

#endif	/* CAMERACONTROL_H */

