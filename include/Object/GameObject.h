/* 
 * File:   GameObject.h
 * Author: victor
 *
 * Created on 28 de enero de 2015, 16:43
 */

#ifndef GAMEOBJECT_H
#define	GAMEOBJECT_H


#include <Ogre.h>
#include <OIS/OIS.h>
using namespace Ogre;

class GameObjectManager;

class GameObject{
    
    private:    

        void init(SceneManager* sce, const String& mesh, Vector3 pos);
        void init(SceneManager* sce, const String& name, const String& mesh, Vector3 pos);

    protected:
        SceneNode* _node;
        SceneManager* _sceneMgr;

        Vector3 _color;    

        std::vector<GameObject*> _childs;

    public:
        
        String tag;

        GameObject(GameObjectManager* gom, const String& mesh);
        GameObject(GameObjectManager* gom, const String& mesh, Vector3 pos);
        GameObject(GameObjectManager* gom, const String& name, const String& mesh);
        GameObject(GameObjectManager* gom, const String& name, const String& mesh, Vector3 pos);

        virtual ~GameObject();

        virtual void update(Real delta);
        virtual void keyPressed(const OIS::KeyEvent &e);
        virtual void keyReleased(const OIS::KeyEvent &e);

        virtual void buttonPressed( const OIS::JoyStickEvent &e, int button );
        virtual void buttonReleased( const OIS::JoyStickEvent &e, int button );
        virtual void axisMoved( Real rel, int axis ); 
        virtual void povMoved( const OIS::JoyStickEvent &e, int index);

        void setPosition(Vector3 vec);
        void setScale(Vector3 vec);
        void setScale(float scale);
        virtual void translate(Real x, Real y, Real z);
        virtual void translate(Vector3 vec);
        virtual void lookAt(Vector3 vec);
        virtual void rotX(Real degree);
        virtual void rotY(Real degree);
        virtual void rotZ(Real degree);

        Real distance(GameObject* g);
        Real distance(Vector3 point);

        void setColor(Real r, Real g, Real b, Real a);
        void setTexture(String texture);
        void setVisible(bool visible);

        void addChild(GameObject* go);
        GameObject* getChild(const String& name);
        GameObject* getChild(int num);
        std::vector<GameObject*> getChilds();

        bool collide(GameObject* object);
        bool sphereCollide(GameObject* object);
        AxisAlignedBox collision(GameObject* object);

        const Vector3& getPosition();
        const Vector3& getScale();
        const Vector3& getColor();
        const String& getName();

        void setCastShadows(bool shadow);

        Vector3 getSize();
        AxisAlignedBox getWorldBoundingBox();
        Sphere getWorldBoundingSfere();

        AxisAlignedBox getBoundingBox();

        SceneManager* getSceneManager();    
        SceneNode* getNode();
};


#endif	/* GAMEOBJECT_H */

