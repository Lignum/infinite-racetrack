/* 
 * File:   Script.h
 * Author: victor
 *
 * Created on 28 de mayo de 2015, 15:02
 */

#ifndef SCRIPT_H
#define	SCRIPT_H


#include <Ogre.h>
#include <OIS/OIS.h>

using namespace Ogre; 

class GameObjectManager;

class Script{
    
    public:
        Script(GameObjectManager* gom);
        
        virtual void update(Real delta);
        
        virtual void keyPressed(const OIS::KeyEvent &e);
        virtual void keyReleased(const OIS::KeyEvent &e);

        virtual void buttonPressed( const OIS::JoyStickEvent &e, int button );
        virtual void buttonReleased( const OIS::JoyStickEvent &e, int button );
        virtual void axisMoved( Real rel, int axis ); 
        virtual void povMoved( const OIS::JoyStickEvent &e, int index);
    
};

#endif	/* SCRIPT_H */

