#ifndef GameState_H
#define GameState_H

#include <Ogre.h>
#include <OIS/OIS.h>
 
#include "GameManager.h"
#include "InputManager.h"

#include "Sound/TrackManager.h"
#include "Sound/SoundFXManager.h"
#include <SDL/SDL.h>
#include <SDL/SDL_mixer.h>


#include "Object/GameObjectManager.h"

#include "GUI/GUIContainer.h"
#include "GUI/Cursor.h"
#include "GUI/GUIImage.h"
#include "GUI/Button.h"
#include "GUI/Text.h"
#include "GUI/TextField.h"
#include "GUI/Slider.h"

#include "GUI/Console.h"

// Clase abstracta de estado básico.
// Definición base sobre la que extender
// los estados del juego.
class GameState {
    
    protected:        
                
        GameObjectManager* _gom;

        //SOUND
        TrackManager* _pTrackManager;
        SoundFXManager* _pSoundFXManager;
        TrackPtr _mainTrack;
        SoundFXPtr _simpleEffect;
    
    
    public:
        GameState() {}

        // Gestión básica del estado.
        virtual void enter () = 0;
        virtual void exit () = 0;
        virtual void pause () = 0;
        virtual void resume () = 0;

        // Gestión básica para el tratamiento
        // de eventos de teclado y ratón.
        virtual void keyPressed (const OIS::KeyEvent &e) = 0;
        virtual void keyReleased (const OIS::KeyEvent &e) = 0;

        virtual void buttonPressed( const OIS::JoyStickEvent &arg, int button ) = 0;
        virtual void buttonReleased( const OIS::JoyStickEvent &arg, int button ) = 0;
        virtual void axisMoved( const OIS::JoyStickEvent &arg, int axis ) = 0;
        virtual void sliderMoved( const OIS::JoyStickEvent &e, int index) = 0;
        virtual void povMoved( const OIS::JoyStickEvent &e, int index) = 0;
        virtual void vector3Moved( const OIS::JoyStickEvent &e, int index) = 0;

        virtual void mouseMoved (const OIS::MouseEvent &e) = 0; 
        virtual void mousePressed (const OIS::MouseEvent &e, 
                                   OIS::MouseButtonID id) = 0;
        virtual void mouseReleased (const OIS::MouseEvent &e, 
                                    OIS::MouseButtonID id) = 0;

        // Gestión básica para la gestión
        // de eventos antes y después de renderizar un frame.
        virtual bool frameStarted (const Ogre::FrameEvent& evt) = 0;
        virtual bool frameEnded (const Ogre::FrameEvent& evt) = 0;

        // Gestión básica de transiciones.
        void changeState (GameState* state) { 
            GameManager::getSingletonPtr()->changeState(state);
        }
        void pushState (GameState* state) {
            GameManager::getSingletonPtr()->pushState(state);
        }
        void popState () {
            GameManager::getSingletonPtr()->popState();
        }

};

#endif
