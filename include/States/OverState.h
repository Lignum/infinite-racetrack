/* 
 * File:   OverState.h
 * Author: victor
 *
 * Created on 7 de julio de 2015, 17:54
 */

#ifndef OVERSTATE_H
#define	OVERSTATE_H

#include <Ogre.h>
#include <OIS/OIS.h>

#include "GameState.h"


#include "File/ReadWrite.h"
#include "File/Score.h"

class OverState : public Ogre::Singleton<OverState>, public GameState{
    
    private:
        
        std::vector<Score> _vec;
        
        String _score;        
        int s, s2;
        
        int _nPos;
        
        int _sel;
        
        void records();
        
        void createUI();
        void pressButton();
        
        void submitScore();
        void exitToMenu();
        
        
        
    public:
        
        OverState();

        void enter ();
        void exit ();
        void pause ();
        void resume ();

        void keyPressed (const OIS::KeyEvent &e);
        void keyReleased (const OIS::KeyEvent &e);

        void buttonPressed( const OIS::JoyStickEvent &e, int button );
        void buttonReleased( const OIS::JoyStickEvent &e, int button );
        void axisMoved( const OIS::JoyStickEvent &e, int axis ); 
        void sliderMoved( const OIS::JoyStickEvent &e, int index);
        void povMoved( const OIS::JoyStickEvent &e, int index);
        void vector3Moved( const OIS::JoyStickEvent &e, int index);

        void mouseMoved (const OIS::MouseEvent &e);
        void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
        void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);

        bool frameStarted (const Ogre::FrameEvent& evt);
        bool frameEnded (const Ogre::FrameEvent& evt);
        
        void setScore(float score);

        // Heredados de Ogre::Singleton.
        static OverState& getSingleton ();
        static OverState* getSingletonPtr ();

    protected:
        Ogre::Root* _root;
        Ogre::SceneManager* _sceneMgr;
        Ogre::Viewport* _viewport;
        Ogre::Camera* _camera;

        bool _exitGame;
};

#endif	/* OVERSTATE_H */

