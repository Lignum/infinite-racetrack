#ifndef PlayState_H
#define PlayState_H

#include <Ogre.h>
#include <OIS/OIS.h>

#include "GameState.h"


#include "Object/Infinite/RoadManager.h"
#include "Object/Infinite/CarControler.h"
#include "Object/Infinite/CameraControl.h"


class PlayState : public Ogre::Singleton<PlayState>, public GameState{
    
    private:
        Real _delta;
        
        bool _generate;
        
        RoadManager* _roadMng;
        CarControler* _car;
        CameraControl* _camCon;
        
        Real _camSpeed;
        
        //###### INPUT ######
        Real _axis0, _axis1, _axis2, _axis3;
        int _povU;
        
        int _counter;
        
        void createUI();
        
        void pauseState();
        void counterLights();
        void gameOver();
        
    public:

        PlayState ();

        void enter ();
        void exit ();
        void pause ();
        void resume ();

        void keyPressed (const OIS::KeyEvent &e);
        void keyReleased (const OIS::KeyEvent &e);

        void buttonPressed( const OIS::JoyStickEvent &e, int button );
        void buttonReleased( const OIS::JoyStickEvent &e, int button );
        void axisMoved( const OIS::JoyStickEvent &e, int axis ); 
        void sliderMoved( const OIS::JoyStickEvent &e, int index);
        void povMoved( const OIS::JoyStickEvent &e, int index);
        void vector3Moved( const OIS::JoyStickEvent &e, int index);

        void mouseMoved (const OIS::MouseEvent &e);
        void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
        void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);

        bool frameStarted (const Ogre::FrameEvent& evt);
        bool frameEnded (const Ogre::FrameEvent& evt);

        // Heredados de Ogre::Singleton.
        static PlayState& getSingleton ();
        static PlayState* getSingletonPtr ();
        
        void addCar();
        void exitState();

    protected:

        Ogre::Root* _root;
        Ogre::SceneManager* _sceneMgr;
        Ogre::Viewport* _viewport;
        Ogre::Camera* _camera;

        bool _exitGame;
};

#endif
