#ifndef IntroState_H
#define IntroState_H

#include <Ogre.h>
#include <OIS/OIS.h>

#include "GameState.h"


class IntroState : public Ogre::Singleton<IntroState>, public GameState{
    
    private:
        
        
        
        bool initSDL();
        
    public:
        IntroState();

        void enter ();
        void exit ();
        void pause ();
        void resume ();

        void keyPressed (const OIS::KeyEvent &e);
        void keyReleased (const OIS::KeyEvent &e);

        void buttonPressed( const OIS::JoyStickEvent &e, int button );
        void buttonReleased( const OIS::JoyStickEvent &e, int button );
        void axisMoved( const OIS::JoyStickEvent &e, int axis );
        void sliderMoved( const OIS::JoyStickEvent &e, int index);
        void povMoved( const OIS::JoyStickEvent &e, int index);
        void vector3Moved( const OIS::JoyStickEvent &e, int index);

        void mouseMoved (const OIS::MouseEvent &e);
        void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
        void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);

        bool frameStarted (const Ogre::FrameEvent& evt);
        bool frameEnded (const Ogre::FrameEvent& evt);

        // Heredados de Ogre::Singleton.
        static IntroState& getSingleton ();
        static IntroState* getSingletonPtr ();

    protected:
        Ogre::Root* _root;
        Ogre::SceneManager* _sceneMgr;
        Ogre::Viewport* _viewport;
        Ogre::Camera* _camera;

        bool _exitGame;
};

#endif
