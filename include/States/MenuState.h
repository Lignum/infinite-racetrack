/* 
 * File:   MenuState.h
 * Author: victor
 *
 * Created on 9 de febrero de 2015, 15:54
 */

#ifndef MENUSTATE_H
#define	MENUSTATE_H

#include <Ogre.h>
#include <OIS/OIS.h>

#include "GameState.h"


#include "Object/Infinite/RoadManager.h"
#include "Object/Infinite/CarControler.h"
#include "Object/Infinite/CameraControl.h"

class MenuState : public Ogre::Singleton<MenuState>, public GameState{
    
    private:
                
        Real _delta;
        
        //###### INPUT ######
        Real _axis0, _axis1, _axis2, _axis3;
        int _sel;
        
        
        void createUI();
        
        void play();
        void records();
        void credits();
        void exitGame();
        void pressButton();
        
    public:

        MenuState () {}

        void enter ();
        void exit ();
        void pause ();
        void resume ();

        void keyPressed (const OIS::KeyEvent &e);
        void keyReleased (const OIS::KeyEvent &e);

        void buttonPressed( const OIS::JoyStickEvent &e, int button );
        void buttonReleased( const OIS::JoyStickEvent &e, int button );
        void axisMoved( const OIS::JoyStickEvent &e, int axis ); 
        void sliderMoved( const OIS::JoyStickEvent &e, int index);
        void povMoved( const OIS::JoyStickEvent &e, int index);
        void vector3Moved( const OIS::JoyStickEvent &e, int index);

        void mouseMoved (const OIS::MouseEvent &e);
        void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
        void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);

        bool frameStarted (const Ogre::FrameEvent& evt);
        bool frameEnded (const Ogre::FrameEvent& evt);

        // Heredados de Ogre::Singleton.
        static MenuState& getSingleton ();
        static MenuState* getSingletonPtr ();

    protected:

        Ogre::Root* _root;
        Ogre::SceneManager* _sceneMgr;
        Ogre::Viewport* _viewport;
        Ogre::Camera* _camera;

        bool _exitGame;
};

#endif	/* MENUSTATE_H */

