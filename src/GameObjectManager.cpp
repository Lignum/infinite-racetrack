#include "Object/GameObjectManager.h"

GameObjectManager::GameObjectManager(SceneManager* sce) {
    _sceneMgr = sce;
    
    initPhysics();
}

void GameObjectManager::initPhysics() {
    OgreBulletCollisions::DebugDrawer * _debugDrawer = new OgreBulletCollisions::DebugDrawer();
    _debugDrawer->setDrawWireframe(true);
    
    SceneNode *node = _sceneMgr->getRootSceneNode()->createChildSceneNode("debugNode", Vector3::ZERO);
    node->attachObject(static_cast <SimpleRenderable *>(_debugDrawer));
    
    
    // Creacion del mundo (definicion de los limites y la gravedad) ---
    AxisAlignedBox worldBounds = AxisAlignedBox (
        Vector3 (-10000, -10000, -10000), 
        Vector3 (10000,  10000,  10000));
    Vector3 gravity = Vector3(0, -15, 0);

    _world = new OgreBulletDynamics::DynamicsWorld(_sceneMgr,worldBounds, gravity);
    _world->setDebugDrawer(_debugDrawer);
    _world->setShowDebugShapes(false);  // Muestra los collision shape
}


void GameObjectManager::addObject(GameObject* object) {
    _gameObjects.push_back(object);
}

void GameObjectManager::addScript(Script* scri) {
    _scripts.push_back(scri);
}


GameObject* GameObjectManager::getObjectByName(const String& name) {
    for(long unsigned int i = 0; i<_gameObjects.size(); i++){
        if(_gameObjects[i]->getName() == name){
            return _gameObjects[i];
        }
    }
    
    return NULL;
}

GameObject* GameObjectManager::getObject(const int num) {
    return _gameObjects[num];
}

PhysicGameObject* GameObjectManager::getPhysicObjectByName(const String& name) {
    return static_cast<PhysicGameObject*>(getObjectByName(name));
}

PhysicGameObject* GameObjectManager::getPhysicObject(const int num) {
    return static_cast<PhysicGameObject*>(getObject(num));
}

std::vector<GameObject*> GameObjectManager::getObjects() {
    return _gameObjects;
}

OgreBulletDynamics::DynamicsWorld* GameObjectManager::getWorld() {
    return _world;
}


void GameObjectManager::clear() {
    
    for(long unsigned int i = 0; i<_gameObjects.size(); i++){
        delete _gameObjects[i];
    }
    _gameObjects.clear();
}

void GameObjectManager::destroyObject(const String& name){
    int pos = -1;
    for(long unsigned int i = 0; i<_gameObjects.size(); i++){
        if(_gameObjects[i]->getName() == name){
            pos = i;
            break;
        }
    }
    
    destroyObject(pos);
}

void GameObjectManager::destroyObject(int i) {
    delete _gameObjects[i];
    _gameObjects.erase(_gameObjects.begin()+i);
}

void GameObjectManager::destroyObject(int start, int end) {
    for(int i=start; i<end; i++){   
        delete _gameObjects[i];
    }
    _gameObjects.erase(_gameObjects.begin()+start, _gameObjects.begin()+end);
}

void GameObjectManager::setVisible(bool visible) {
    for(long unsigned int i = 0; i<_gameObjects.size(); i++){
        _gameObjects[i]->setVisible(visible);
    }
}


void GameObjectManager::update(Real delta) {
    for(long unsigned int i = 0; i<_gameObjects.size(); i++){
        _gameObjects[i]->update(delta);
    }
    
    for(long unsigned int i = 0; i<_scripts.size(); i++){
        _scripts[i]->update(delta);
    }
        
    _world->stepSimulation(delta);
}

void GameObjectManager::keyPressed(const OIS::KeyEvent& e) {
    for(long unsigned int i = 0; i<_gameObjects.size(); i++){
        _gameObjects[i]->keyPressed(e);
    }
    
    for(long unsigned int i = 0; i<_scripts.size(); i++){
        _scripts[i]->keyPressed(e);
    }
}

void GameObjectManager::keyReleased(const OIS::KeyEvent& e) {
    for(long unsigned int i = 0; i<_gameObjects.size(); i++){
        _gameObjects[i]->keyReleased(e);
    }
    
    for(long unsigned int i = 0; i<_scripts.size(); i++){
        _scripts[i]->keyReleased(e);
    }
}

void GameObjectManager::buttonPressed(const OIS::JoyStickEvent& e, int button) {
    for(long unsigned int i = 0; i<_gameObjects.size(); i++){
        _gameObjects[i]->buttonPressed(e, button);
    }
    
    for(long unsigned int i = 0; i<_scripts.size(); i++){
        _scripts[i]->buttonPressed(e, button);
    }
}

void GameObjectManager::buttonReleased(const OIS::JoyStickEvent& e, int button) {
    for(long unsigned int i = 0; i<_gameObjects.size(); i++){
        _gameObjects[i]->buttonReleased(e, button);
    }
    
    for(long unsigned int i = 0; i<_scripts.size(); i++){
        _scripts[i]->buttonReleased(e, button);
    }
}

void GameObjectManager::axisMoved(Real rel, int axis) {
    for(long unsigned int i = 0; i<_gameObjects.size(); i++){
        _gameObjects[i]->axisMoved(rel, axis);
    }
    
    for(long unsigned int i = 0; i<_scripts.size(); i++){
        _scripts[i]->axisMoved(rel, axis);
    }
}

void GameObjectManager::povMoved(const OIS::JoyStickEvent& e, int index) {
    for(long unsigned int i = 0; i<_gameObjects.size(); i++){
        _gameObjects[i]->povMoved(e, index);
    }
    
    for(long unsigned int i = 0; i<_scripts.size(); i++){
        _scripts[i]->povMoved(e, index);
    }
}

SceneManager* GameObjectManager::getSceneManager() {
    return _sceneMgr;
}

