#include "Object/Script.h"

#include "Object/GameObjectManager.h"

Script::Script(GameObjectManager* gom) {
    gom->addScript(this);
}

void Script::update(Real delta) {

}

void Script::keyPressed(const OIS::KeyEvent& e) {

}

void Script::keyReleased(const OIS::KeyEvent& e) {

}

void Script::buttonPressed(const OIS::JoyStickEvent& e, int button) {

}

void Script::buttonReleased(const OIS::JoyStickEvent& e, int button) {

}

void Script::axisMoved(Real rel, int axis) {

}

void Script::povMoved(const OIS::JoyStickEvent& e, int index) {

}
