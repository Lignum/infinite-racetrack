#include "States/PauseState.h"

#include "States/PlayState.h"
#include "States/MenuState.h"

template<> PauseState* Ogre::Singleton<PauseState>::msSingleton = 0;

PauseState::PauseState() {

}

void PauseState::enter (){
    _root = Ogre::Root::getSingletonPtr();

    // Se recupera el gestor de escena y la cámara.
    _sceneMgr = _root->getSceneManager("SceneManager");
    _camera = _sceneMgr->getCamera("IntroCamera");
    _viewport = _root->getAutoCreatedWindow()->getViewport(0);
    // Nuevo background colour.

    _exitGame = false;

    createUI();
    
}

void PauseState::createUI() {
    GUIContainer::createGroup("pause");
    
    GUIContainer::getGroup("pause")->setZOrder(641);
    
    GUIContainer::createImage("pause","back","PauseBack.png");
    
    GUIContainer::createText("pause","txtPause","PAUSE");
    GUIContainer::getText("pause","txtPause")->setAlignment(TextAreaOverlayElement::Center);
    GUIContainer::getText("pause","txtPause")->setPosition(683, 200);
    GUIContainer::getText("pause","txtPause")->setCharHeight(0.12);
    
    
    GUIContainer::createButton("pause","btnResume","Button1.png","Button1Focus.png","Button1.png","RESUME");
    GUIContainer::getButton("pause","btnResume")->setPosition(557.5,310);
        
    GUIContainer::createButton("pause","btnExit","Button1.png","Button1Focus.png","Button1.png","EXIT");
    GUIContainer::getButton("pause","btnExit")->setPosition(557.5,410);    
    
    
}


void PauseState::exit (){
}

void PauseState::pause (){
}

void PauseState::resume (){
}

bool PauseState::frameStarted (const Ogre::FrameEvent& evt){
    
    if(_sel != 0){
        GUIContainer::getGroup("pause")->nextButton(_sel);
        _sel = 0;
    }
    
    return true;
}

bool PauseState::frameEnded (const Ogre::FrameEvent& evt){
    
    
    GUIContainer::update(evt.timeSinceLastFrame);
    
    
    if (_exitGame)
        return false;

    return true;
}

void PauseState::keyPressed (const OIS::KeyEvent &e) {
    
    if(e.key == OIS::KC_UP){
        _sel = -1;
    }else if(e.key == OIS::KC_DOWN){
        _sel = 1;
    }else if(e.key == OIS::KC_SPACE || e.key == OIS::KC_RETURN){
        pressButton();
    }
    
}

void PauseState::keyReleased (const OIS::KeyEvent &e){
    
    
    
    /*
     * #########################################################################
     * DEBUG
     * #########################################################################
     */
    if (e.key == OIS::KC_BACK) {
        _exitGame = true;
    }
}


void PauseState::buttonPressed(const OIS::JoyStickEvent& e, int button) {
    if(button == 1){
        pressButton();
    }
    
    
    if(button == 9){
        resumePlay();
    }
}

void PauseState::buttonReleased(const OIS::JoyStickEvent& e, int button) {

}

void PauseState::axisMoved(const OIS::JoyStickEvent& e, int axis) {

}

void PauseState::sliderMoved(const OIS::JoyStickEvent& e, int index) {

}

void PauseState::povMoved(const OIS::JoyStickEvent& e, int index) {
    _sel = 0;
    
    if(e.state.mPOV[index].direction == e.state.mPOV[index].Centered){
        _sel = 0;
    }else if(e.state.mPOV[index].direction == e.state.mPOV[index].North){
        _sel = -1;
    }else if(e.state.mPOV[index].direction == e.state.mPOV[index].South){
        _sel = 1;
    }
}

void PauseState::vector3Moved(const OIS::JoyStickEvent& e, int index) {

}

void PauseState::mouseMoved (const OIS::MouseEvent &e){
    GUIContainer::moveCursor(e.state.X.abs, e.state.Y.abs);
}

void PauseState::mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id){
    
}

void PauseState::mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id){
    
}

PauseState* PauseState::getSingletonPtr (){
    return msSingleton;
}

PauseState& PauseState::getSingleton (){ 
  assert(msSingleton);
  return *msSingleton;
}

void PauseState::pressButton() {

    GUIContainer::getButton("pause","btnResume")->enter(&PauseState::resumePlay, *this);
    GUIContainer::getButton("pause","btnExit")->enter(&PauseState::exitToMenu, *this);
}

void PauseState::resumePlay() {
    
    GUIContainer::getGroup("pause")->setAllvisible(false);
    
    popState();
}

void PauseState::exitToMenu() {
    resumePlay();
    PlayState::getSingletonPtr()->exitState();
}
