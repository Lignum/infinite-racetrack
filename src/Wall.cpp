#include "Object/Infinite/Wall.h"
#include "Object/GameObject.h"
#include "Object/Infinite/Road.h"
#include "Object/GameObjectManager.h"

Wall::Wall(GameObjectManager* gom, const String& mesh, int type, Vector3 pos, int rot) : GameObject(gom, mesh) {
    
    _gom = gom;
    
    switch(type){
        case 0:
            _col1 = new PhysicGameObject(gom, true, "stoneWallS1Col.mesh", pos, Vector3(0,rot,0));
            _col2 = new PhysicGameObject(gom, true, "stoneWallS1Col2.mesh", pos, Vector3(0,rot,0));
            break;
            
        case 1:
            _col1 = new PhysicGameObject(gom, true, "stoneWallR1Col.mesh", pos, Vector3(0,rot,0));
            //_col2 = new PhysicGameObject(gom, true, "stoneWallR1Col2.mesh", pos, Vector3(0,rot,0));
            break;
        case -1:
            _col1 = new PhysicGameObject(gom, true, "stoneWallL1Col.mesh", pos, Vector3(0,rot,0));
            //_col2 = new PhysicGameObject(gom, true, "stoneWallL2Col2.mesh", pos, Vector3(0,rot,0));
            break;
            
            
        case 2:
            _col1 = new PhysicGameObject(gom, true, "stoneWallR2Col.mesh", pos, Vector3(0,rot,0));
            _col2 = new PhysicGameObject(gom, true, "stoneWallR2Col2.mesh", pos, Vector3(0,rot,0));
            break;
        case -2:
            _col1 = new PhysicGameObject(gom, true, "stoneWallL2Col.mesh", pos, Vector3(0,rot,0));
            _col2 = new PhysicGameObject(gom, true, "stoneWallL2Col2.mesh", pos, Vector3(0,rot,0));
            break;
            
        case 3:
            _col1 = new PhysicGameObject(gom, true, "stoneWallR3Col.mesh", pos, Vector3(0,rot,0));
            _col2 = new PhysicGameObject(gom, true, "stoneWallR3Col2.mesh", pos, Vector3(0,rot,0));
            break;
        case -3:
            _col1 = new PhysicGameObject(gom, true, "stoneWallL3Col.mesh", pos, Vector3(0,rot,0));
            _col2 = new PhysicGameObject(gom, true, "stoneWallL3Col2.mesh", pos, Vector3(0,rot,0));
            break;
    }
    

    //if(mesh == MESH_R1) addChild(new GameObject(gom, "stoneWallR1.mesh"));
    //if(mesh == Road::MESH_R2 || mesh == Road::MESH_L2);
    //if(mesh == Road::MESH_R3 || mesh == Road::MESH_L3);
    
    _col1->setVisible(false);
    
    if(type != 1 && type != -1) _col2->setVisible(false);
    
}

Wall::~Wall() {
    //_gom->destroyObject(_col1->getName());
    //_gom->destroyObject(_col2->getName());
    
}
