#include "Object/Infinite/Car.h"
#include "GUI/Console.h"
#include "Object/GameObjectManager.h"
#include "Object/Infinite/Road.h"
#include "States/GameManager.h"

std::vector<Road*>* Car::_roads;
CarControler* Car::_car;
int Car::_numCars;

Car::Car(GameObjectManager* gom, Vector3 pos, Vector2 dir, int firstRoad, float speed, float lane) : 
PhysicGameObject(gom, false, "peugeot.mesh", pos, Vector3(0,0,0), 0.5, 1000000, 1000, 
        PhysicGameObject::SHAPE_TYPE_CUBE, Vector3(1, 1, 2), Vector3(0, 1, 1)){
    
    tag = "car";
    
    setScale(0.6);
    
    _gom = gom;
    
    _rigidBody->disableDeactivation();
    _rigidBody->setKinematicObject(true);
    
    _dir = Vector3(dir.x,0, dir.y);
    _speed = speed;
    _lane = lane;
    
    _lastObject = NULL;
    _nextCol = firstRoad;
    
    _turn = false;
    _upDown = 0;
    _radio = -1;
    
    _numCars++;
    
    
    //_speed = 0;
    addChild(new GameObject(gom,"CamaroWhell.mesh", Vector3(1.2,0.5,2.1)));
    addChild(new GameObject(gom,"CamaroWhell.mesh", Vector3(-1.2,0.5,2.1)));    
    
    addChild(new GameObject(gom,"CamaroWhell.mesh", Vector3(1.2,0.5,-1.8)));
    addChild(new GameObject(gom,"CamaroWhell.mesh", Vector3(-1.2,0.5,-1.8)));
    
   
}

Car::~Car() {
    _numCars--;
}

void Car::update(Real delta) {
    
    //std::cout << "Car update" << std::endl;
    
    this->getChild(0)->rotX(55*_speed*delta);
    this->getChild(1)->rotX(55*_speed*delta);
    this->getChild(2)->rotX(55*_speed*delta);
    this->getChild(3)->rotX(55*_speed*delta);
    
    //Console::log(_dir);
    
    //std::cout << "Car: " << _speed << std::endl;
    if(_speed != 0){
        this->translate(_dir*_speed*delta);
        lookAt(getPosition()-_dir);
        
        if(!_turn && _upDown == 0){

    //std::cout << "Car 11" << std::endl;

            if(_roads[0][_nextCol] != NULL){
                
                
    //std::cout << "Car: " << _roads[0][_nextCol]->getPosition() << " aa: " << getPosition()-Vector3(0,2,0) << std::endl;

                if(_roads[0][_nextCol]->getWorldBoundingBox().contains(getPosition()-Vector3(0,5,0))){

    //std::cout << "Car 22" << std::endl;
                    _lastObject = _roads[0][_nextCol];

                    _nextCol++;

                    if(_nextCol >= (int)_roads[0].size()){
                        _nextCol = 0;
                    }

    //std::cout << "Car 23" << std::endl;
                    if(_lastObject->getSize() != 0){
                        _turn = true;
                    }else{
                        if(_lastObject->tag == "up"){
                            _upDown = 1;
                        }else if(_lastObject->tag == "down"){
                            _upDown = -1;
                        }
                    }
                    
                    if(!_turn && _upDown == 0){
                        straight();
                    }

                }
            }

        }else{
            if(_lastObject->getWorldBoundingBox().contains(Vector3(getPosition()-Vector3(0,2,0) ))){
                
                
    //std::cout << "Car 111" << std::endl;
                if(_turn){
                    turn();
                }else{
                    ramp(_upDown);
                }
    //std::cout << "Car 211" << std::endl;
            }else{
    //std::cout << "Car 311" << std::endl;
                if(_turn){
    //std::cout << "Car 321" << std::endl;
                    straight();
                }
                _turn = false;
                _upDown = 0;
                _radio = -1;
            }
        }


    //std::cout << "Car 33" << std::endl;
        if(getPosition().z-_maxDistance > _car->getPosition().z){
            _gom->destroyObject(getName());
        }

        collision();
    }else{
        translate(_dir * _speedCollision * delta);
        
        if(_speedCollision >= 0){
            _speedCollision -= 0.5*delta;
        }else{
            _speedCollision = 0;
        }
        
    }
    
    //std::cout << "Car update end" << std::endl;
    
}

void Car::turn() {
        
    float x,z;
    
    switch(_lastObject->getDirection()){
        case Road::DIR_N:
        case Road::DIR_S:
            if(_radio == -1){
                _radio = getPosition().distance(_lastObject->getCenter());
                _initZ = getPosition().x;
            }
            
            x = getPosition().x-_initZ;            
            
            z = sqrt(fabs(pow(_radio,2)-pow(x,2)));
            
            if(_lastObject->getSize() < 0){
                z *= -1;
                x *= -1;
            }
            
            
            break;
            
        case Road::DIR_E:
        case Road::DIR_W:
            
            if(_radio == -1){
                _radio = getPosition().distance(_lastObject->getCenter());
                _initZ = getPosition().z;
            }


            z = getPosition().z-_initZ;

            x = -sqrt(fabs(pow(_radio,2)-pow(z,2)));

            if(_lastObject->getSize() < 0) z *= -1;

            break;
    }
    
    
    _dir = Vector3(-z,0,x).normalisedCopy();
    //lookAt(getPosition()+_dir);

}

void Car::ramp(int dir) {
    //_dir = Vector3(_dir.x, dir*0.05, _dir.z).normalisedCopy();
    _dir = Vector3(_dir.x, dir*0.035, _dir.z).normalisedCopy();
    //Console::log(_dir);
}

void Car::straight() {
    switch(_lastObject->getDirection()){
        case Road::DIR_N:
            _dir = Vector3(0,0,-1);
            break;
        case Road::DIR_S:
            _dir = Vector3(0,0,1);
            break;            
        case Road::DIR_E:
            _dir = Vector3(1,0,0);
            break;
        case Road::DIR_W:
            _dir = Vector3(-1,0,0);
            break;
    }
    
    //std::cout << "Car 322" << std::endl;
    rePos();
}

void Car::rePos() {
    if(_roads[0][_nextCol] != NULL){
        if(_roads[0][_nextCol]->getSize() == 0){
            switch(_roads[0][_nextCol]->getDirection()){
                case Road::DIR_N:
                    translate((_roads[0][_nextCol]->getPosition().x + _lane - getPosition().x)/2 ,0 ,0 );
                        //setPosition(Vector3(_roads[0][_nextCol]->getPosition().x + _lane, 0, getPosition().z));
                    break;
                case Road::DIR_S:
                    translate((_roads[0][_nextCol]->getPosition().x - _lane - getPosition().x)/2 ,0 ,0 );
                    break;            
                case Road::DIR_E:
                    translate(0 ,0 ,(_roads[0][_nextCol]->getPosition().z + _lane - getPosition().z)/2);
                    break;
                case Road::DIR_W:
                    translate(0 ,0 ,(_roads[0][_nextCol]->getPosition().z - _lane - getPosition().z)/2);
                    break;
            }
        }
    }
}

void Car::collision() {
    
    
    btCollisionWorld *bulletWorld = _world->getBulletCollisionWorld();
    int numManifolds = bulletWorld->getDispatcher()->getNumManifolds();

    for (int i=0;i<numManifolds;i++) {
        btPersistentManifold* contactManifold = bulletWorld->getDispatcher()->getManifoldByIndexInternal(i);
        contactManifold->getBody0();
        
        btCollisionObject* obA = const_cast<btCollisionObject*>(contactManifold->getBody0());
        btCollisionObject* obB = const_cast<btCollisionObject*>(contactManifold->getBody1());      
        
        OgreBulletCollisions::Object *obOB_A = _world->findObject(obA);
        OgreBulletCollisions::Object *obOB_B = _world->findObject(obB);        
        
        if ((obOB_A == _world->findObject(_node)) || (obOB_B == _world->findObject(_node))) {
            Ogre::SceneNode* drain = _car->getNode();
            OgreBulletCollisions::Object *obDrain = _world->findObject(drain);

            if ((obOB_A == obDrain) || (obOB_B == obDrain)) {
                _speed = 0;
                _speedCollision = 0.5;
                _dir = _car->getLinearVelocity();
                _dir.y = 0;
                
                _car->collide();
            } 
        }
    }
}

void Car::setRoads(std::vector<Road*>* roads) {
    _roads = roads;
}

void Car::setCarControler(CarControler* car) {
    _car = car;
}

int Car::getNumCars() {
    return _numCars;
}

