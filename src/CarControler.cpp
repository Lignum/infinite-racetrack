#include "Object/Infinite/CarControler.h"
#include "GUI/Console.h"
#include "GUI/GUIContainer.h"


CarControler::CarControler(GameObjectManager* gom, Vector3 pos) : GameObject(gom, "Camaro.mesh"){
    
    _gom = gom;
    
    //delete _node;
    const float scale = 0.6;
    this->setScale(scale);
    
    
    //_Chassis = gom->getSceneManager()->createEntity("chassis", "Camaro.mesh");
    //_node = gom->getSceneManager()->getRootSceneNode()->createChildSceneNode ();
    
    //SceneNode *chassisnode = _node->createChildSceneNode();
    //chassisnode->attachObject (_Chassis);
    //chassisnode->setPosition (chassisShift);
    //chassisnode->yaw(Degree(180));
    
    const Ogre::Vector3 chassisShift(0, 0.75, 0);

    BoxCollisionShape* chassisShape = new BoxCollisionShape(Ogre::Vector3(0.7, 0.5, 1.7));
    CompoundCollisionShape* compound = new CompoundCollisionShape();
    compound->addChildShape(chassisShape, chassisShift);

    _CarChassis = new WheeledRigidBody("carChassis", gom->getWorld());
    
    _CarChassis->setShape (_node, compound, 0.6, 0.6, 1000, pos, Quaternion(0,0,1,0));
    _CarChassis->setDamping(0.2, 0.2);
    _CarChassis->disableDeactivation();

    _Tuning = new VehicleTuning(20.2, 4.4, 2.3, 500.0, 10.5);
    _VehicleRayCaster = new VehicleRayCaster(gom->getWorld());
    _Vehicle = new RaycastVehicle(_CarChassis, _Tuning, _VehicleRayCaster);

    _Vehicle->setCoordinateSystem(0, 1, 2);

    Ogre::Vector3 wheelDirectionCS0(0,-1,0);
    Ogre::Vector3 wheelAxleCS(-1,0,0);

    for (size_t i = 0; i < 4; i++) {
        _Wheels[i] = gom->getSceneManager()->createEntity("wheel" + i, "CamaroWhell.mesh");
        _Wheels[i]->setCastShadows(true);
       
        _WheelNodes[i] = gom->getSceneManager()->getRootSceneNode()->createChildSceneNode();
        _WheelNodes[i]->attachObject (_Wheels[i]);
        
        _WheelNodes[i]->setScale(Vector3(scale,scale,scale));
    }

    bool isFrontWheel = true;
    
    
    float connectionHeight = 0.9f;
    float connectionWidth = 0.6f;
    float connectionLong = 1.5f;
    
    Ogre::Vector3 connectionPointCS0 (connectionWidth-(0.3*gWheelWidth), connectionHeight, connectionLong-gWheelRadius);
    _Vehicle->addWheel(_WheelNodes[0], connectionPointCS0, wheelDirectionCS0, wheelAxleCS, gSuspensionRestLength, gWheelRadius, isFrontWheel, gWheelFriction, gRollInfluence);

    connectionPointCS0 = Ogre::Vector3(-connectionWidth+(0.3*gWheelWidth), connectionHeight, connectionLong-gWheelRadius);
    _Vehicle->addWheel(_WheelNodes[1], connectionPointCS0, wheelDirectionCS0, wheelAxleCS, gSuspensionRestLength, gWheelRadius, isFrontWheel, gWheelFriction, gRollInfluence);
    

    isFrontWheel = false;
    connectionPointCS0 = Ogre::Vector3(-connectionWidth+(0.3*gWheelWidth), connectionHeight,-connectionLong+gWheelRadius+0.25);
    _Vehicle->addWheel(_WheelNodes[2], connectionPointCS0, wheelDirectionCS0, wheelAxleCS, gSuspensionRestLength, gWheelRadius, isFrontWheel, gWheelFriction, gRollInfluence);
    this->addChild(new GameObject(gom, "CamaroWhell.mesh", connectionPointCS0*Vector3(2,0.5,2)));

    connectionPointCS0 = Ogre::Vector3(connectionWidth-(0.3*gWheelWidth), connectionHeight,-connectionLong+gWheelRadius+0.25);
    _Vehicle->addWheel(_WheelNodes[3], connectionPointCS0, wheelDirectionCS0, wheelAxleCS, gSuspensionRestLength, gWheelRadius, isFrontWheel, gWheelFriction, gRollInfluence);
    this->addChild(new GameObject(gom, "CamaroWhell.mesh", connectionPointCS0*Vector3(2,0.5,2)));
    
    _WheelNodes[2]->setVisible(false);
    _WheelNodes[3]->setVisible(false);


    _butThrottle = false;
    _butBreak = false;

    _drift = false;
    _drifting = false;
    _driftDir = 0;

    _steer = 0;
    
    _acceleration = 0.005;
    _maxSpeed = 280*5; //Km/h
    _speed = 0;
    
    
    Ogre::ParticleSystem* ps = _sceneMgr->createParticleSystem("Ps","ringOfFire");
    _WheelNodes[2]->attachObject(ps);
    Ogre::ParticleSystem* ps2 = _sceneMgr->createParticleSystem("Ps2","ringOfFire");
    _WheelNodes[3]->attachObject(ps2);
    
    _sceneMgr->getParticleSystem("Ps")->setVisible(false);
    _sceneMgr->getParticleSystem("Ps2")->setVisible(false);
    
    
    
    
    _Smotor = SoundFXManager::getSingletonPtr()->load("motor.wav");
    _Smotor->setVolumen(0, 0);
    _Smotor->playChannel(-1, 0);
    
    
    _Sdrift = SoundFXManager::getSingletonPtr()->load("motor.wav");
    //_Sdrift->setVolumen(10);
}

void CarControler::destroy() {
}

void CarControler::update(Real delta) {
    
    _kmh = _Vehicle->getBulletVehicle()->getCurrentSpeedKmHour();
    
    if(_kmh < 0.4 && _kmh > -0.4) _kmh = 0;
    
    float k = (int)(_kmh*100);
    k = k/100;
   
   
    _Smotor->setVolumen(0, _kmh*_maxVolume);
    _Sdrift->setVolumen(10, 0);
    
    aceleration(delta);

    steer(delta);
    
    drift(delta);
    
    if(_drifting){
        
        _Sdrift->setVolumen(10, 50);
        _Sdrift->playChannel(0, 10);
        _sceneMgr->getParticleSystem("Ps")->setVisible(true);
        _sceneMgr->getParticleSystem("Ps2")->setVisible(true);
    }else{
        
        
        _sceneMgr->getParticleSystem("Ps")->setVisible(false);
        _sceneMgr->getParticleSystem("Ps2")->setVisible(false);
    }
    
    this->getChild(0)->rotX(50*_kmh*delta);
    this->getChild(1)->rotX(50*_kmh*delta);
    
    
    
    GUIContainer::getGUIImage("play","veloA")->setRotation(-(fabs(_kmh)-40)*180/350);
    //reflect();
}

void CarControler::aceleration(Real delta) {
    if(_butThrottle){ //Boton Acelerador
        _speed = _acceleration*(_maxSpeed-_kmh);
        
    }else if(_drift && _kmh>0){//Si no acelera y derrapa
        _speed = -1;
        
    }else if(_butBreak){
                
        _speed = -_acceleration*((_maxSpeed/2)-_kmh);
    
    }else{
        _speed = 0;
        _Vehicle->getBulletVehicle()->setBrake(50, 0);
        _Vehicle->getBulletVehicle()->setBrake(50, 1);
        _Vehicle->getBulletVehicle()->setBrake(50, 2);
        _Vehicle->getBulletVehicle()->setBrake(50, 3);
    }
    
     
    //Aplicar aceleracion
    _Vehicle->applyEngineForce (gEngineForce*_speed, 0);
    _Vehicle->applyEngineForce (gEngineForce*_speed, 1);
    
    //if(!_drift || (_drift && !_butThrottle)){
        _Vehicle->applyEngineForce (gEngineForce*_speed, 2);
        _Vehicle->applyEngineForce (gEngineForce*_speed, 3);
    //}
}

void CarControler::steer(Real delta) {
    Real steer = _steer;
    
    //Aplicar giro
    if(!_drift){//si no esta derrapando esta limitado por la velocidad
        steer *= ((_maxSpeed/((_kmh+1)*_maxSpeed))*5);
        if(steer>0.8) steer=0.8;
        if(steer<-0.8) steer=-0.8;
        
        if(_kmh<=0) steer *= -1;
        
        _Vehicle->setSteeringValue(steer, 0);
        _Vehicle->setSteeringValue(steer, 1);
    }else{
        _Vehicle->setSteeringValue(steer, 0);
        _Vehicle->setSteeringValue(steer, 1);
        
    }
}

void CarControler::drift(Real delta) {

    if(_drift){//Derrape acelerando
                
            //Console::log(_driftDir);
        if(!_drifting){//Mueve las ruedas traseras para el derrape
            if(_steer>0 && _driftDir>=0){
                _dir = -5*delta;
            }
            if(_steer<0 && _driftDir<=0){
                _dir = 5*delta;
            }
            _drifting = true;
        }
        
        /*if(_driftDir<0 && _steering<0){
            _dir = 0.0005*delta;
            
            if(_driftDir+_dir>0){
                 _driftDir = 0;
                 _drift = false;
            }
        }
        if(_driftDir>0 && _steering>0){
            _dir = -0.0005*delta;
            
            if(_driftDir+_dir<0){
                 _driftDir = 0;
                 _drift = false;
            }
        }*/
        
        
        _driftDir += _dir;
        
        float maxDrift = _maxDriftDir;
        if(_kmh<100){
            maxDrift = (_kmh*_maxDriftDir)/200;
        }
        if(_driftDir<-maxDrift) _driftDir = -maxDrift;        
        if(_driftDir>maxDrift) _driftDir = maxDrift;
        
        
//        if(_driftDir<-(_maxDriftDir/_maxSteer)*-_steering) _driftDir = -(_maxDriftDir/_maxSteer)*-_steering;
//        if(_driftDir>(_maxDriftDir/_maxSteer)*-_steering) _driftDir = (_maxDriftDir/_maxSteer)*-_steering;
                
    }else{
        
        if(_driftDir<0){
            _driftDir += 5*delta;
            _Vehicle->getBulletVehicle()->getWheelInfo(0).m_frictionSlip += 25*delta;
            _Vehicle->getBulletVehicle()->getWheelInfo(1).m_frictionSlip += 25*delta;
        }
        if(_driftDir>0){
            _driftDir -= 5*delta;
            _Vehicle->getBulletVehicle()->getWheelInfo(0).m_frictionSlip += 25*delta;
            _Vehicle->getBulletVehicle()->getWheelInfo(1).m_frictionSlip += 25*delta;
        }
        
        if(_driftDir<0.1 && _driftDir>-0.1){
            _driftDir = 0;
            _Vehicle->getBulletVehicle()->getWheelInfo(0).m_frictionSlip = gWheelFriction;
            _Vehicle->getBulletVehicle()->getWheelInfo(1).m_frictionSlip = gWheelFriction;
            
            _drifting = false;
        }
    }
    
    _Vehicle->setSteeringValue(_driftDir, 2);
    _Vehicle->setSteeringValue(_driftDir, 3);
}


Vector3 CarControler::getLinearVelocity() {
    Vector3 di = static_cast<Vector3>(_Vehicle->getBulletVehicle()->getRigidBody()->getLinearVelocity());
    
    return di;
}

void CarControler::keyPressed(const OIS::KeyEvent& e) {
    
    if(e.key == OIS::KC_X){
        if(_kmh>0){
            _drift = true;
            _Vehicle->getBulletVehicle()->getWheelInfo(0).m_frictionSlip = 5;
            _Vehicle->getBulletVehicle()->getWheelInfo(1).m_frictionSlip = 5;
        }
    }

    if(e.key == OIS::KC_UP){
        _butThrottle = true;
    }else if(e.key == OIS::KC_DOWN){
        _butBreak = true;
    }
    
    if(e.key == OIS::KC_LEFT){
        _steer = _maxSteer;
    }else if(e.key == OIS::KC_RIGHT){
        _steer = -_maxSteer;
    }
    
    
    
}

void CarControler::keyReleased(const OIS::KeyEvent& e) {
    if(e.key == OIS::KC_UP){
        _butThrottle = false;
    }else if(e.key == OIS::KC_DOWN){
        _butBreak = false;
    }
    
    if(e.key == OIS::KC_LEFT || e.key == OIS::KC_RIGHT){
        _steer = 0;
    }
    
    if(e.key == OIS::KC_X){        
        _drift = false;
        _drifting = false;
    }
}

void CarControler::buttonPressed(const OIS::JoyStickEvent& e, int button) {
    switch(button){
        case 1:
            _butThrottle = true;
            break;
        case 2:
            _butBreak = true;
            break;
        case 5:
            if(_kmh>0){
                _drift = true;
                _Vehicle->getBulletVehicle()->getWheelInfo(0).m_frictionSlip = 5;
                _Vehicle->getBulletVehicle()->getWheelInfo(1).m_frictionSlip = 5;
            }
            //_Vehicle->getBulletVehicle()->getWheelInfo(2).m_frictionSlip = 2;
            //_Vehicle->getBulletVehicle()->getWheelInfo(3).m_frictionSlip = 2;
            //_Vehicle->getBulletVehicle()->setBrake(100,2);
            //_Vehicle->getBulletVehicle()->setBrake(100,3);
            break;
    }
}

void CarControler::buttonReleased(const OIS::JoyStickEvent& e, int button) {
    switch(button){
        case 1:
            _butThrottle = false;
            break;
        case 2:
            _butBreak = false;
            break;
        case 5:
            _drift = false;
            _drifting = false;
            //_Vehicle->getBulletVehicle()->getWheelInfo(0).m_frictionSlip = gWheelFriction;
            //_Vehicle->getBulletVehicle()->getWheelInfo(1).m_frictionSlip = gWheelFriction;
            //_Vehicle->getBulletVehicle()->getWheelInfo(2).m_frictionSlip = gWheelFriction;
            //_Vehicle->getBulletVehicle()->getWheelInfo(3).m_frictionSlip = gWheelFriction;
            break;
    }
}

void CarControler::axisMoved(Real rel, int axis) {
    
    
    switch(axis){
        case 0:
            _steer = -rel*_maxSteer;
            break;
        case 1:
            
            break;
    }
    
}

void CarControler::povMoved(const OIS::JoyStickEvent& e, int index) {

}

Real CarControler::getSteer() {
    return _steer;
}

float CarControler::getKmh() {
    return _kmh;
}

void CarControler::collide() {
    _speed = -100;
    //_butThrottle = false;
    _drift = false;
    
    
    _Vehicle->getBulletVehicle()->setBrake(200, 0);
    _Vehicle->getBulletVehicle()->setBrake(200, 1);
    _Vehicle->getBulletVehicle()->setBrake(200, 2);
    _Vehicle->getBulletVehicle()->setBrake(200, 3);
}

void CarControler::silence() {
    _Smotor->setVolumen(0,0);
    _Sdrift->setVolumen(10,0);
}

Vector3 CarControler::getPosition() {
    return _node->getPosition();
}
