#include <OgreBullet/Collisions/Shapes/OgreBulletCollisionsCompoundShape.h>

#include "Object/PhysicGameObject.h"
#include "Object/GameObjectManager.h"

PhysicGameObject::PhysicGameObject(GameObjectManager* gom, bool stati, const String& mesh, Vector3 pos) : GameObject(gom, mesh) {
    _shapeType = SHAPE_TYPE_MESH;
    init(gom->getWorld(), stati, mesh, pos, Vector3(0,0,0), 0.1, 0.8, 5.0);
}

PhysicGameObject::PhysicGameObject(GameObjectManager* gom, bool stati, const String& mesh, Vector3 pos, Vector3 rot) : GameObject(gom, mesh){
    _shapeType = SHAPE_TYPE_MESH;
    init(gom->getWorld(), stati, mesh, pos, rot, 0.1, 0.8, 5.0);
}

PhysicGameObject::PhysicGameObject(GameObjectManager* gom, bool stati, const String& mesh, 
        Vector3 pos, Vector3 rot, Real bouncines, Real friccion, Real mass) : GameObject(gom, mesh) {
    _shapeType = SHAPE_TYPE_MESH;
    init(gom->getWorld(), stati, mesh, pos, rot, bouncines, friccion, mass);
}

PhysicGameObject::PhysicGameObject(GameObjectManager* gom, bool stati, const String& mesh, 
        Vector3 pos, Vector3 rot, Real bouncines, Real friccion, Real mass, int type, Vector3 shapeSize, Vector3 shapePos) : GameObject(gom, mesh) {
    _shapeType = type;
    _shapeSize = shapeSize;
    _shapePos = shapePos;
    init(gom->getWorld(), stati, mesh, pos, rot, bouncines, friccion, mass);
}

PhysicGameObject::PhysicGameObject(GameObjectManager* gom, bool stati, const String& name, const String& mesh, Vector3 pos) : GameObject(gom, name, mesh) {
    _shapeType = SHAPE_TYPE_MESH;
    init(gom->getWorld(), stati, mesh, pos, Vector3(0,0,0), 0.1, 0.8, 5.0);
}

void PhysicGameObject::init(OgreBulletDynamics::DynamicsWorld* world, bool stati, const String& mesh, Vector3 pos, Vector3 rot, Real bouncines, Real friccion, Real mass) {
    
    _world = world;
    _static = stati;
    _mesh = mesh;
    
    _node->setPosition(pos);
    
    _node->pitch(Ogre::Degree(rot.x));
    _node->yaw(Ogre::Degree(rot.y));
    _node->roll(Ogre::Degree(rot.z));
    //rotZ(rot.z);
    
    _bouncines = bouncines;
    _friccion = friccion;
    _mass = mass;
    
    initPhysics();
    
}

void PhysicGameObject::initPhysics() {
    
    
    Vector3 position = _node->getPosition();
    Quaternion rotation = _node->getOrientation();
    //_node->setScale(1.01,1.01,1.01);
        
    Ogre::Entity* ent = _sceneMgr->createEntity(_mesh);
    
    _rigidBody = new OgreBulletDynamics::RigidBody(_node->getName() + "Rigi", _world);
    if(!_static){
        if(_shapeType == SHAPE_TYPE_MESH){
            OgreBulletCollisions::CollisionShape *shape = OgreBulletCollisions::StaticMeshToShapeConverter(ent).createConvex();
            _rigidBody->setShape(_node, shape, _bouncines, _friccion, _mass, position, rotation);
            
        }else if(_shapeType == SHAPE_TYPE_CUBE){


            OgreBulletCollisions::BoxCollisionShape* box = new OgreBulletCollisions::BoxCollisionShape(_shapeSize);
            OgreBulletCollisions::CompoundCollisionShape* compound = new OgreBulletCollisions::CompoundCollisionShape();
            compound->addChildShape(box, _shapePos);
            _rigidBody->setShape(_node, compound, _bouncines, _friccion, _mass, position, rotation);
            
        }
        //OgreBulletCollisions::BoxCollisionShape* box = new OgreBulletCollisions::BoxCollisionShape(Ogre::Vector3(1,1,1));
        //_rigidBody->setShape(_node, box, _bouncines, _friccion, _mass, position, rotation);
        
    }else{
        //OgreBulletCollisions::CollisionShape *shape = OgreBulletCollisions::StaticMeshToShapeConverter(ent).createConvex();
        OgreBulletCollisions::TriangleMeshCollisionShape *shape = OgreBulletCollisions::StaticMeshToShapeConverter(ent).createTrimesh();
        _rigidBody->setStaticShape(_node, shape, _bouncines, _friccion, position, rotation);
      
    }
}

PhysicGameObject::~PhysicGameObject() {
    
    delete _rigidBody;
}

void PhysicGameObject::update(Real delta) {
}


void PhysicGameObject::setPosition(Vector3 vec) {
    
    if(!_rigidBody->isKinematicObject() && !_rigidBody->isStaticObject() ){
        delete _rigidBody;
        GameObject::setPosition(vec);
        initPhysics();
    }else{
        GameObject::setPosition(vec);
    }
}

void PhysicGameObject::translate(Real x, Real y, Real z) {
    
    if(!_rigidBody->isKinematicObject() && ! _rigidBody->isStaticObject() ){
        delete _rigidBody;
        GameObject::translate(x,y,z);
        initPhysics();
    }else{
        GameObject::translate(x,y,z);
    }
}

void PhysicGameObject::translate(Vector3 vec) {
    if(!_rigidBody->isKinematicObject() && ! _rigidBody->isStaticObject() ){
        delete _rigidBody;
        GameObject::translate(vec);
        initPhysics();
    }else{
        GameObject::translate(vec);
    }
}

void PhysicGameObject::rotX(Real degree) {
    if(!_rigidBody->isKinematicObject() && ! _rigidBody->isStaticObject() ){
        delete _rigidBody;
        GameObject::rotX(degree);
        initPhysics();
    }else{
        GameObject::rotX(degree);
    }
}

void PhysicGameObject::rotY(Real degree) {
    if(!_rigidBody->isKinematicObject() && ! _rigidBody->isStaticObject() ){
        delete _rigidBody;
        GameObject::rotY(degree);
        initPhysics();
    }else{
        GameObject::rotY(degree);
    }
}

void PhysicGameObject::rotZ(Real degree) {
    if(!_rigidBody->isKinematicObject() && ! _rigidBody->isStaticObject() ){
        delete _rigidBody;
        GameObject::rotZ(degree);
        initPhysics();
    }else{
        GameObject::rotZ(degree);
    }
}

void PhysicGameObject::lookAt(Vector3 pos) {
    if(!_rigidBody->isKinematicObject() && ! _rigidBody->isStaticObject() ){
        delete _rigidBody;
        GameObject::_node->lookAt(pos, SceneNode::TS_WORLD);
        initPhysics();
    }else{
        GameObject::_node->lookAt(pos, SceneNode::TS_WORLD);
    }
}

bool PhysicGameObject::isCollision(PhysicGameObject* object) {     
    
    btCollisionWorld *bulletWorld = _world->getBulletCollisionWorld();
    int numManifolds = bulletWorld->getDispatcher()->getNumManifolds();

    for (int i=0;i<numManifolds;i++) {
        btPersistentManifold* contactManifold = bulletWorld->getDispatcher()->getManifoldByIndexInternal(i);
        contactManifold->getBody0();;
                
        btCollisionObject* obA = const_cast<btCollisionObject*>(contactManifold->getBody0());
        btCollisionObject* obB = const_cast<btCollisionObject*>(contactManifold->getBody1());      
        
        OgreBulletCollisions::Object *obOB_A = _world->findObject(obA);
        OgreBulletCollisions::Object *obOB_B = _world->findObject(obB);        
        
        if ((obOB_A == _world->findObject(_node)) || (obOB_B == _world->findObject(_node))) {
            Ogre::SceneNode* drain = object->getNode();
            OgreBulletCollisions::Object *obDrain = _world->findObject(drain);

            if ((obOB_A == obDrain) || (obOB_B == obDrain)) {
                return true;
            } 
        }
    }
    return false;
    
}

PhysicGameObject* PhysicGameObject::collision(GameObjectManager* gom) {
    btCollisionWorld *bulletWorld = _world->getBulletCollisionWorld();
    int numManifolds = bulletWorld->getDispatcher()->getNumManifolds();

    for (int i=0;i<numManifolds;i++) {
        btPersistentManifold* contactManifold = bulletWorld->getDispatcher()->getManifoldByIndexInternal(i);
        contactManifold->getBody0();;
                
        btCollisionObject* obA = const_cast<btCollisionObject*>(contactManifold->getBody0());
        btCollisionObject* obB = const_cast<btCollisionObject*>(contactManifold->getBody1());      
        
        OgreBulletCollisions::Object *obOB_A = _world->findObject(obA);
        OgreBulletCollisions::Object *obOB_B = _world->findObject(obB);        
        
        if (obOB_A == _world->findObject(_node)) {
            String name = obOB_B->getName();
            name = name.erase(name.size()-4,4);
            return gom->getPhysicObjectByName(name);
        }else if (obOB_B == _world->findObject(_node)){
            String name = obOB_A->getName();
            name = name.erase(name.size()-4,4);
            return gom->getPhysicObjectByName(name);            
        }
    }
    return NULL;
}

OgreBulletDynamics::RigidBody* PhysicGameObject::getRigidBody() {
    return _rigidBody;
}
