#include "Object/Infinite/Road.h"
#include "GUI/Console.h"
#include "Object/Infinite/CheckPoint.h"
#include "Object/Infinite/Wall.h"

const String Road::MESH_S1 = "RoadS1.mesh";
const String Road::MESH_R1 = "RoadR1.mesh";
const String Road::MESH_L1 = "RoadL1.mesh";
const String Road::MESH_R2 = "RoadR2.mesh";
const String Road::MESH_L2 = "RoadL2.mesh";
const String Road::MESH_R3 = "RoadR3.mesh";
const String Road::MESH_L3 = "RoadL3.mesh";

Road::Road(GameObjectManager* gom, const String& mesh, Vector3 pos, int dir) : PhysicGameObject(gom, true, mesh, pos) {

    setCastShadows(false);
    
    _dir = dir;
    _size = 0;
    
    tag = mesh;
    
    addChild(new Wall(gom, "stoneWallS1.mesh", _size, getPosition(), 0));
    
}

Road::Road(GameObjectManager* gom, const String& mesh, Road* last, bool check, int upDown) : PhysicGameObject(gom, true, mesh, last->getPosition())  {
    
    tag = mesh;
    
    
    
    setCastShadows(false);
    
    _size = (int)getBoundingBox().getSize().z;
    
    _size /= DISTANCE;
    
    _size = round(_size);    
    
    Real size = _size;
    
    size = 0.5 + (0.5*size);
    
    if(mesh == MESH_S1){
        _size = 0;
    }
    if(mesh == MESH_L1 || mesh == MESH_L2 || mesh == MESH_L3){
        _size *= -1;
    }
    
    
    Real lastSize = fabs(last->getSize());
    
    if(lastSize>1){
        lastSize = 0.5*(lastSize-1);
    }else {
        lastSize = 0;
    }
    
    int lasDir = 1;
    if((size<-1 || last->getSize()<0) && last->getSize()<1){
        lasDir = -1;
    }
    
    int degre = 0;
    
    switch(last->getDirection()){
        case DIR_N:
            translate(0,0,-DISTANCE*(size+lastSize));
            translate(-DISTANCE*lastSize*lasDir,0,0);
            break;
        case DIR_E:
            translate(DISTANCE*(size+lastSize),0,0);
            translate(0,0,-DISTANCE*lastSize*lasDir);
            degre = -90;
            rotY(-90);
            break;
        case DIR_S:
            translate(0,0,DISTANCE*(size+lastSize));
            translate(DISTANCE*lastSize*lasDir,0,0);
            degre = -180;
            rotY(-180);
            break;
        case DIR_W:
            translate(-DISTANCE*(size+lastSize),0,0);
            translate(0,0,DISTANCE*lastSize*lasDir);
            degre = -270;
            rotY(-270);
            break;
    }
    
    

    _dir = last->getDirection();
    
    if(_size>0){
        _dir+=1;
        if(_dir>3) _dir = 0;
    }else if(_size<0){
        _dir-=1;
        if(_dir<0) _dir = 3;
    }
    
    size -= 1;
    switch(_dir){
        case DIR_N:
            translate(0,0,-DISTANCE*size);
            break;
        case DIR_E:
            translate(DISTANCE*size,0,0);
            break;
        case DIR_S:
            translate(0,0,DISTANCE*size);
            break;
        case DIR_W:
            translate(-DISTANCE*size,0,0);
            break;
    }
    
    
    calculateCenter();
    
    if(last->tag == "up"){
        translate(0,0.7,0);
        //translate(0,1.05,0);
    }else if(last->tag == "down"){
        translate(0,-0.7,0);       
        //translate(0,-1.05,0);
    }
    
    switch(upDown){
        case 1:
            rotX(2);
            translate(0,0.7,0);
            //translate(0,1.05,0);
            
            tag = "up";
            break;
        case -1:            
            rotX(-2);
            translate(0,-0.7,0);
            //translate(0,-1.05,0);
            tag = "down";
            
            break;
    }
    
    
    delete _rigidBody;
    PhysicGameObject::initPhysics();

    if(mesh == MESH_S1) addChild(new Wall(gom, "stoneWallS1.mesh", _size, getPosition(), degre));
    
    if(mesh == MESH_R1 || mesh == MESH_L1) addChild(new Wall(gom, "stoneWallR1.mesh", _size, getPosition(), degre));
    if(mesh == MESH_R2 || mesh == MESH_L2) addChild(new Wall(gom, "stoneWallR2.mesh", _size, getPosition(), degre));
    if(mesh == MESH_R3 || mesh == MESH_L3) addChild(new Wall(gom, "stoneWallR3.mesh", _size, getPosition(), degre));
    
    if(mesh == MESH_L1 || mesh == MESH_L2 || mesh == MESH_L3) getChild(0)->setScale(Vector3(-1,1,1));
    
    if(check) new CheckPoint(gom, getPosition(), degre);
    
}

void Road::calculateCenter() {
    _center = getPosition();
    
    
    Vector3 v = Vector3(1,0,1);
    
    switch(_dir){
        case DIR_N:
        case DIR_S:
             
            v = Vector3(1,0,-1);
            
            if(_size < 0) v.x *= -1;

            _center += v*fabs(_size)*Road::DISTANCE/2;            
            break;
            
        case DIR_E:
        case DIR_W:
             
            if(_size < 0) v.x *= -1;

            _center += v*fabs(_size)*Road::DISTANCE/2;
            break;
    }
    
}

int Road::getDirection() {
    return _dir;
}

Vector3 Road::next(int dir) {
    
    dir = _dir+dir;
    if(dir>3) dir = 0;
    if(dir<0) dir = 3;
    
    Vector3 res = getPosition();
    
    
    switch(_dir){
        case DIR_N:
            res += Vector3(0,0,-DISTANCE*fabs(_size));
            break;
        case DIR_E:
            res += Vector3(DISTANCE*fabs(_size),0,0);
            break;
        case DIR_S:
            res += Vector3(0,0,DISTANCE*fabs(_size));
            break;
        case DIR_W:
            res += Vector3(-DISTANCE*fabs(_size),0,0);
            break;
    }
    
    switch(dir){
        case DIR_N:
            return res + Vector3(0,0,-DISTANCE);
            break;
        case DIR_E:
            return res + Vector3(DISTANCE,0,0);
            break;
        case DIR_S:
            return res + Vector3(0,0,DISTANCE);
            break;
        case DIR_W:
            return res + Vector3(-DISTANCE,0,0);
            break;
    }
    
    return Vector3(0,0,0);
}

Real Road::getSize() {
    return _size;
}

Vector3 Road::getCenter() {
    return _center;
}
