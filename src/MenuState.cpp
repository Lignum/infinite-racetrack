#include "States/MenuState.h"

#include "States/IntroState.h"
#include "States/PauseState.h"
#include "States/PlayState.h"


#include "File/ReadWrite.h"
#include "File/Score.h"

template<> MenuState* Ogre::Singleton<MenuState>::msSingleton = 0;

void MenuState::enter (){
    _root = Ogre::Root::getSingletonPtr();

    // Se recupera el gestor de escena y la cámara.
    _sceneMgr = _root->getSceneManager("SceneManager");
    _camera = _sceneMgr->getCamera("IntroCamera");
    
    _viewport = _root->getAutoCreatedWindow()->getViewport(0);
    //_viewport = _root->getAutoCreatedWindow()->addViewport(_camera);
    
    _exitGame = false;
        
    _pTrackManager = TrackManager::getSingletonPtr();
    _pSoundFXManager = SoundFXManager::getSingletonPtr();
            
    //_mainTrack = _pTrackManager->load("137290__fran-ky__126-numerology-metal-3-c.wav");
    
    //_mainTrack->play();
    _simpleEffect = _pSoundFXManager->load("MetalPing.aiff");
    
    _sceneMgr->setSkyBox(true, "tropical",1000,false);
    
    /* 
     * #########################################################################
     * LUCES
     * #########################################################################
     */
    
    
    _sceneMgr->setShadowTechnique(Ogre::SHADOWTYPE_STENCIL_ADDITIVE);
    _sceneMgr->setShadowFarDistance(1000);
    
    _sceneMgr->setShadowColour(Ogre::ColourValue(0.5, 0.5, 0.5) );
    _sceneMgr->setAmbientLight(Ogre::ColourValue(0.5, 0.5, 0.5));
    
    Ogre::Light* light = _sceneMgr->createLight("Light1");
    light->setPosition(-11, 10,-10);
    light->setDiffuseColour(0.9, 0.9, 0.9);
    light->setType(Ogre::Light::LT_DIRECTIONAL);
    light->setDirection(Ogre::Vector3(-1,-1,0.5));
    light->setSpotlightInnerAngle(Ogre::Degree(25.0f));
    light->setSpotlightOuterAngle(Ogre::Degree(60.0f));
    light->setSpotlightFalloff(5.0f);
    light->setCastShadows(true);
    
    
    //############################################################
    _gom = new GameObjectManager(_sceneMgr);
    
    
    //############################################################
    _camera->setPosition(0,0.1,0.1);
    _camera->lookAt(0.5,-0.25,1);
    
    new GameObject(_gom,"Plane.mesh", Vector3(0,-10.01,0));
    
    for(int i=0; i<10; i++){
        
        new GameObject(_gom, "RoadS1.mesh", Vector3(0,-10, Road::DISTANCE*i));
    }
    
    createUI();
    
}

void MenuState::createUI() {
    GUIContainer::createGroup("menu");
    
    GUIContainer::createImage("menu", "ima", "menu.png");
    
    
    GUIContainer::createButton("menu","btnPlay","Button1.png","Button1Focus.png","Button1.png","PLAY");
    GUIContainer::getButton("menu","btnPlay")->setPosition(50,210);    
    
    GUIContainer::createButton("menu","btnRecords","Button1.png","Button1Focus.png","Button1.png","RECORDS");
    GUIContainer::getButton("menu","btnRecords")->setPosition(50,310);
    
    
    GUIContainer::createButton("menu","btnCredits","Button1.png","Button1Focus.png","Button1.png","CREDITS");
    GUIContainer::getButton("menu","btnCredits")->setPosition(50,410);
    
    GUIContainer::createButton("menu","btnExit","Button1.png","Button1Focus.png","Button1.png","EXIT");
    GUIContainer::getButton("menu","btnExit")->setPosition(50,510);
    
    
    
    GUIContainer::createGroup("record");
    GUIContainer::getGroup("record")->setZOrder(642);
    
    GUIContainer::createImage("record", "score", "score.png");
    GUIContainer::getGroup("record")->setAllvisible(false);
    
    
    GUIContainer::createGroup("credits");
    GUIContainer::getGroup("credits")->setZOrder(642);
    
    GUIContainer::createImage("credits", "back", "score.png");
    GUIContainer::getGroup("credits")->setAllvisible(false);
}

void MenuState::exit (){
    _sceneMgr->clearScene();
    _root->getAutoCreatedWindow()->removeAllViewports();
}

void MenuState::pause(){
}

void MenuState::resume(){
    
}

bool MenuState::frameStarted(const Ogre::FrameEvent& evt){
    
    _delta = (1.0 / evt.timeSinceLastFrame);
    Console::setStatic("FPS", "FPS" ,(int)_delta);
    
    _gom->update(evt.timeSinceLastFrame);
    
    
    if(_sel != 0){
        GUIContainer::getGroup("menu")->nextButton(_sel);
        _sel = 0;
    }
    return true;
}

bool MenuState::frameEnded(const Ogre::FrameEvent& evt){
    
    GUIContainer::update(evt.timeSinceLastFrame);
    
    if (_exitGame)
        return false;
  
    return true;
}

void MenuState::keyPressed(const OIS::KeyEvent &e){
    
    if(e.key == OIS::KC_UP){
        _sel = -1;
    }else if(e.key == OIS::KC_DOWN){
        _sel = 1;
    }else if(e.key == OIS::KC_SPACE || e.key == OIS::KC_RETURN){
        pressButton();
    }
    
    
    
    /*
     * #########################################################################
     * DEBUG
     * #########################################################################
     */
    
    if(e.key == OIS::KC_P){
        changeState(PlayState::getSingletonPtr());
    }
    
    if (e.text == 186) {
        bool visible = !Console::isVisible();
        Console::setVisible(visible);
        _sceneMgr->showBoundingBoxes(!_sceneMgr->getShowBoundingBoxes());
        //_gom->getWorld()->setShowDebugContactPoints(visible);
        _gom->getWorld()->setShowDebugShapes(visible);
    }
    
    if (e.key == OIS::KC_Q) {
        Console::setVisible(!Console::isVisible());
        
    }
}

void MenuState::keyReleased(const OIS::KeyEvent &e){
    //_gom->keyReleased(e);
    
    /*
     * #########################################################################
     * DEBUG
     * #########################################################################
     */
    if (e.key == OIS::KC_0) {
        _exitGame = true;
    }
}

void MenuState::buttonPressed(const OIS::JoyStickEvent& e, int button) {
    //_gom->buttonPressed(e, button);
    
    if(button == 1){
        pressButton();
    }
}

void MenuState::buttonReleased(const OIS::JoyStickEvent& e, int button) {
    //_gom->buttonReleased(e, button);
}

void MenuState::axisMoved(const OIS::JoyStickEvent& e, int axis) {
    Real ax = e.state.mAxes[axis].abs;
    ax /= 32768;
    ax = (int)(ax*10000);
    ax /= 10000;
    if(ax>0){        
        ax += 0.0001;
    }
    if(ax>1) ax=1;
    if(ax<-1) ax=-1;
    
    if(ax<0.02 && ax>-0.02) ax=0;
    
    //_gom->axisMoved(ax, axis);
    
    switch(axis){
        case 0:
            _axis0 = ax;
            break;
        case 1:
            _axis1 = ax;
            break;
        case 2:
            _axis2 = ax;
            break;
        case 3:
            _axis3 = ax;
            break;
            
    }
}

void MenuState::sliderMoved(const OIS::JoyStickEvent& e, int index) {
}

void MenuState::povMoved(const OIS::JoyStickEvent& e, int index) {
    //_gom->povMoved(e, index);
    
    _sel = 0;
    
    if(e.state.mPOV[index].direction == e.state.mPOV[index].Centered){
        _sel = 0;
    }else if(e.state.mPOV[index].direction == e.state.mPOV[index].North){
        _sel = -1;
    }else if(e.state.mPOV[index].direction == e.state.mPOV[index].South){
        _sel = 1;
    }

}

void MenuState::vector3Moved(const OIS::JoyStickEvent& e, int index) {
}

void MenuState::mouseMoved(const OIS::MouseEvent &e){
    
    //GUIContainer::moveCursor(e.state.X.abs, e.state.Y.abs);
}

void MenuState::mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id){
    //GUIContainer::clickPressed(id);
}

void MenuState::mouseReleased(const OIS::MouseEvent &e, OIS::MouseButtonID id){
    //GUIContainer::clickReleased(id);
    
    /*if(id == OIS::MB_Left){
        GUIContainer::getButton("menu","btnPlay")->enter(&MenuState::play, *this);
        //GUIContainer::getButton("menu","btnCreate")->enter(&MenuState::create, *this);
        //GUIContainer::getButton("menu","btnScore")->enter(&MenuState::showScore, *this);
        //GUIContainer::getButton("menu","btnCredits")->enter(&MenuState::showCredits, *this);        
        GUIContainer::getButton("menu","btnExit")->enter(&MenuState::exitGame, *this);
    }*/
}

MenuState* MenuState::getSingletonPtr (){
    return msSingleton;
}

MenuState& MenuState::getSingleton (){ 
    assert(msSingleton);
    return *msSingleton;
}

void MenuState::pressButton() {
    GUIContainer::getButton("menu","btnPlay")->enter(&MenuState::play, *this);
    GUIContainer::getButton("menu","btnRecords")->enter(&MenuState::records, *this);
    GUIContainer::getButton("menu","btnCredits")->enter(&MenuState::credits, *this);        
    GUIContainer::getButton("menu","btnExit")->enter(&MenuState::exitGame, *this);
}

void MenuState::play() {
    GUIContainer::getGroup("menu")->setAllvisible(false);
    GUIContainer::getGroup("record")->setAllvisible(false);
    GUIContainer::getGroup("credits")->setAllvisible(false);
    changeState(PlayState::getSingletonPtr());
}

void MenuState::records() {
    
    GUIContainer::getGroup("credits")->setAllvisible(false);
    GUIContainer::getGroup("record")->setAllvisible(true);
    
    GUIContainer::createText("record","pos", "N");
    GUIContainer::getText("record","pos")->setCharHeight(0.03);
    GUIContainer::getText("record","pos")->setPosition(500,220);
    
    GUIContainer::createText("record","n", "NAME");
    GUIContainer::getText("record","n")->setCharHeight(0.03);
    GUIContainer::getText("record","n")->setPosition(540,220);
        
    GUIContainer::createText("record","l", "___________________________________________________");
    GUIContainer::getText("record","l")->setCharHeight(0.03);
    GUIContainer::getText("record","l")->setPosition(500,225);
    
    GUIContainer::createText("record","s", "KM");
    GUIContainer::getText("record","s")->setAlignment(TextAreaOverlayElement::Right);
    GUIContainer::getText("record","s")->setCharHeight(0.03);
    GUIContainer::getText("record","s")->setPosition(1250,220);
    
    
    ReadWrite<Score> rw("score.sav");
    std::vector<Score> vec = rw.read();    
    
    std::sort(vec.begin(), vec.end());
    std::reverse(vec.begin(), vec.end());
    
    String name;
    String score;
    float sco;
    String num;
    
    for(unsigned int i=0; i<vec.size(); i++){
        num = StringConverter::toString(i+1);
        
        name = vec[i].name;
        sco = vec[i].score+(vec[i].score2*0.01);
        score = StringConverter::toString(sco);
        
        GUIContainer::createText("record","pos"+num, num+":");
        GUIContainer::getText("record","pos"+num)->setCharHeight(0.03);
        GUIContainer::getText("record","pos"+num)->setPosition(500,220+(i+1)*20);
        
        GUIContainer::createText("record","n"+num, name);
        GUIContainer::getText("record","n"+num)->setText(name);
        GUIContainer::getText("record","n"+num)->setCharHeight(0.03);
        GUIContainer::getText("record","n"+num)->setPosition(540,220+(i+1)*20);
        
        GUIContainer::createText("record","l"+num, "_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _");
        GUIContainer::getText("record","l"+num)->setColor(ColourValue(0.7,0.7,0.7));
        GUIContainer::getText("record","l"+num)->setCharHeight(0.03);
        GUIContainer::getText("record","l"+num)->setPosition(500,225+(i+1)*20);
                
        GUIContainer::createText("record","s"+num, score);
        GUIContainer::getText("record","s"+num)->setText(score);
        GUIContainer::getText("record","s"+num)->setAlignment(TextAreaOverlayElement::Right);
        GUIContainer::getText("record","s"+num)->setCharHeight(0.03);
        GUIContainer::getText("record","s"+num)->setPosition(1250,220+(i+1)*20);
    }
}

void MenuState::credits() {
    
    
    GUIContainer::getGroup("record")->setAllvisible(false);
    GUIContainer::getGroup("credits")->setAllvisible(true);
    
    GUIContainer::createText("credits","txtTitle","CREDITS");
    GUIContainer::getText("credits","txtTitle")->setCharHeight(0.12);
    GUIContainer::getText("credits","txtTitle")->setPosition(540,220);
    
    
    GUIContainer::createText("credits","txtPro","PROGRAMER");
    GUIContainer::getText("credits","txtPro")->setCharHeight(0.04);
    GUIContainer::getText("credits","txtPro")->setPosition(500,300);    
    
    GUIContainer::createText("credits","txtP","- Victor Roldan Armengol");
    GUIContainer::getText("credits","txtP")->setCharHeight(0.03);
    GUIContainer::getText("credits","txtP")->setPosition(500,320);
    
    
    
    GUIContainer::createText("credits","txtModels","3D MODELS");
    GUIContainer::getText("credits","txtModels")->setCharHeight(0.04);
    GUIContainer::getText("credits","txtModels")->setPosition(500,360);    
    
    GUIContainer::createText("credits","txtM","- Victor Roldan Armengol");
    GUIContainer::getText("credits","txtM")->setCharHeight(0.03);
    GUIContainer::getText("credits","txtM")->setPosition(500,380);
    
    
    
    GUIContainer::createText("credits","txtTextures","TEXTURES");
    GUIContainer::getText("credits","txtTextures")->setCharHeight(0.04);
    GUIContainer::getText("credits","txtTextures")->setPosition(500,420);    
    
    GUIContainer::createText("credits","txtT1","- Victor Roldan Armengol");
    GUIContainer::getText("credits","txtT1")->setCharHeight(0.03);
    GUIContainer::getText("credits","txtT1")->setPosition(500,440);
    
    GUIContainer::createText("credits","txtT2","- cgtextures.com");
    GUIContainer::getText("credits","txtT2")->setCharHeight(0.03);
    GUIContainer::getText("credits","txtT2")->setPosition(500,460);
    
    
    
    GUIContainer::createText("credits","txtMusic","MUSIC");
    GUIContainer::getText("credits","txtMusic")->setCharHeight(0.04);
    GUIContainer::getText("credits","txtMusic")->setPosition(500,500);
    
    GUIContainer::createText("credits","txtU","- FoolBoyMedia (freesound.org)");
    GUIContainer::getText("credits","txtU")->setCharHeight(0.03);
    GUIContainer::getText("credits","txtU")->setPosition(500,520);
    
    
    
    GUIContainer::createText("credits","txtSounds","SOUNDS");
    GUIContainer::getText("credits","txtSounds")->setCharHeight(0.04);
    GUIContainer::getText("credits","txtSounds")->setPosition(500,560);
    
    GUIContainer::createText("credits","txtS1","- Robinhood76 (freesound.org)");
    GUIContainer::getText("credits","txtS1")->setCharHeight(0.03);
    GUIContainer::getText("credits","txtS1")->setPosition(500,580);
    
    GUIContainer::createText("credits","txtS2","- smokum (freesound.org)");
    GUIContainer::getText("credits","txtS2")->setCharHeight(0.03);
    GUIContainer::getText("credits","txtS2")->setPosition(500,600);
    
    /*GUIContainer::createText("credits","txtS3","- cgtextures.com");
    GUIContainer::getText("credits","txtS3")->setCharHeight(0.03);
    GUIContainer::getText("credits","txtS3")->setPosition(500,460);*/
}

void MenuState::exitGame() {    
    _exitGame = true;
}
