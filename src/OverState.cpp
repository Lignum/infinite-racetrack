#include "States/OverState.h"

#include "States/PlayState.h"
#include "States/MenuState.h"


#include "File/ReadWrite.h"
#include "File/Score.h"

template<> OverState* Ogre::Singleton<OverState>::msSingleton = 0;

OverState::OverState() {

}

void OverState::enter (){
    
    _root = Ogre::Root::getSingletonPtr();

    // Se recupera el gestor de escena y la cámara.
    _sceneMgr = _root->getSceneManager("SceneManager");
    _camera = _sceneMgr->getCamera("IntroCamera");
    _viewport = _root->getAutoCreatedWindow()->getViewport(0);
    // Nuevo background colour.

    _exitGame = false;
    
    _nPos = -1;    

    createUI();
    records();
    
    if(_nPos == -1){
        GUIContainer::getButton("over","btnSubmit")->setVisible(false);
        GUIContainer::getText("over","txtNew")->setVisible(false);
    }
}

void OverState::setScore(float score) {
        
    s = (int)score;
    s2 = (modf(score, new float)*100);
    
    float k = (int)(score*100);
    k = k/100;

    _score = StringConverter::toString(k);
}

void OverState::createUI() {
    GUIContainer::createGroup("over");
    
    GUIContainer::getGroup("over")->setZOrder(641);
    
    
    GUIContainer::createImage("over","back","PauseBack.png");
    
    GUIContainer::createText("over","txtOver","GAME OVER");
    GUIContainer::getText("over","txtOver")->setAlignment(TextAreaOverlayElement::Center);
    GUIContainer::getText("over","txtOver")->setPosition(683, 100);
    GUIContainer::getText("over","txtOver")->setCharHeight(0.12);
    
    
    
    GUIContainer::createText("over","txtNew","NEW RECORD");
    GUIContainer::getText("over","txtNew")->setAlignment(TextAreaOverlayElement::Center);
    GUIContainer::getText("over","txtNew")->setColor(ColourValue::Green);
    GUIContainer::getText("over","txtNew")->setPosition(202.5, 150);
    GUIContainer::getText("over","txtNew")->setCharHeight(0.05);
    
    GUIContainer::createText("over","txtScore","SCORE");
    GUIContainer::getText("over","txtScore")->setAlignment(TextAreaOverlayElement::Center);
    GUIContainer::getText("over","txtScore")->setPosition(202.5, 180);
    GUIContainer::getText("over","txtScore")->setCharHeight(0.08);
    
    GUIContainer::createText("over","txtScoreL","____________________");
    GUIContainer::getText("over","txtScoreL")->setAlignment(TextAreaOverlayElement::Center);
    GUIContainer::getText("over","txtScoreL")->setPosition(202.5, 210);
    GUIContainer::getText("over","txtScoreL")->setCharHeight(0.03);
    
    
    GUIContainer::createText("over","txtScoreKm", _score);
    GUIContainer::getText("over","txtScoreKm")->setText(_score);
    GUIContainer::getText("over","txtScoreKm")->setAlignment(TextAreaOverlayElement::Center);
    GUIContainer::getText("over","txtScoreKm")->setPosition(202.5, 230);
    
    
    GUIContainer::createButton("over","btnSubmit","Button1.png","Button1Focus.png","Button1.png","SUBMIT");
    GUIContainer::getButton("over","btnSubmit")->setPosition(50,340);
    GUIContainer::getButton("over","btnSubmit")->setFocus(true); 
        
    GUIContainer::createButton("over","btnExit","Button1.png","Button1Focus.png","Button1.png","EXIT");
    GUIContainer::getButton("over","btnExit")->setPosition(50,440);    
    
    GUIContainer::createTextField("over","textf","Button1.png"," ");
    GUIContainer::getTextField("over","textf")->setSelected(true);
    GUIContainer::getTextField("over","textf")->setMaxChar(20);
    GUIContainer::getTextField("over","textf")->setVisible(false);
    
}


void OverState::exit (){
}

void OverState::pause (){
}

void OverState::resume (){
}

bool OverState::frameStarted (const Ogre::FrameEvent& evt){
    
    
    
    if(_sel != 0){
        GUIContainer::getGroup("over")->nextButton(_sel);
        _sel = 0;
    }
    
    if(_nPos == -1){
        GUIContainer::getButton("over","btnSubmit")->setFocus(false); 
        GUIContainer::getButton("over","btnExit")->setFocus(true);
    }
    
    return true;
}

bool OverState::frameEnded (const Ogre::FrameEvent& evt){
    
    
    GUIContainer::update(evt.timeSinceLastFrame);
    
    
    if (_exitGame)
        return false;

    return true;
}

void OverState::keyPressed (const OIS::KeyEvent &e) {
    
    if(_nPos != -1){

        GUIContainer::keyPressed(e);

        String num = StringConverter::toString(_nPos+1);

        if(GUIContainer::getTextField("over","textf")->getText()->getText()[0] == ' '){
            String s = GUIContainer::getTextField("over","textf")->getText()->getText();
            s.erase(s.begin());
            GUIContainer::getTextField("over","textf")->getText()->setText(s);
        }

        GUIContainer::getText("record","n"+num)->setText(GUIContainer::getTextField("over","textf")->getText()->getText());

    }
    
    if(e.key == OIS::KC_UP){
        _sel = -1;
    }else if(e.key == OIS::KC_DOWN){
        _sel = 1;
    }else if(e.key == OIS::KC_SPACE || e.key == OIS::KC_RETURN){
        pressButton();
    }
    
}

void OverState::keyReleased (const OIS::KeyEvent &e){
    
    
   
}


void OverState::buttonPressed(const OIS::JoyStickEvent& e, int button) {
    if(button == 1){
        pressButton();
    }
    
    
    if(button == 9){
        _exitGame = true;
        exitToMenu();
    }
}

void OverState::buttonReleased(const OIS::JoyStickEvent& e, int button) {

}

void OverState::axisMoved(const OIS::JoyStickEvent& e, int axis) {

}

void OverState::sliderMoved(const OIS::JoyStickEvent& e, int index) {

}

void OverState::povMoved(const OIS::JoyStickEvent& e, int index) {
    _sel = 0;
    
    if(e.state.mPOV[index].direction == e.state.mPOV[index].Centered){
        _sel = 0;
    }else if(e.state.mPOV[index].direction == e.state.mPOV[index].North){
        _sel = -1;
    }else if(e.state.mPOV[index].direction == e.state.mPOV[index].South){
        _sel = 1;
    }
}

void OverState::vector3Moved(const OIS::JoyStickEvent& e, int index) {

}

void OverState::mouseMoved (const OIS::MouseEvent &e){
    
}

void OverState::mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id){
    
}

void OverState::mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id){
    
}

OverState* OverState::getSingletonPtr (){
    return msSingleton;
}

OverState& OverState::getSingleton (){ 
  assert(msSingleton);
  return *msSingleton;
}

void OverState::pressButton() {

    if(_nPos != -1){
        GUIContainer::getButton("over","btnSubmit")->enter(&OverState::submitScore, *this);
    }
    GUIContainer::getButton("over","btnExit")->enter(&OverState::exitToMenu, *this);
}

void OverState::records() {

    GUIContainer::getGroup("record")->setAllvisible(true);
    
    GUIContainer::createText("record","pos", "N");
    GUIContainer::getText("record","pos")->setCharHeight(0.03);
    GUIContainer::getText("record","pos")->setPosition(500,220);
        
    GUIContainer::createText("record","n", "NAME");
    GUIContainer::getText("record","n")->setCharHeight(0.03);
    GUIContainer::getText("record","n")->setPosition(540,220);
        
    GUIContainer::createText("record","l", "___________________________________________________");
    GUIContainer::getText("record","l")->setCharHeight(0.03);
    GUIContainer::getText("record","l")->setPosition(500,225);

    GUIContainer::createText("record","s", "KM");
    GUIContainer::getText("record","s")->setAlignment(TextAreaOverlayElement::Right);
    GUIContainer::getText("record","s")->setCharHeight(0.03);
    GUIContainer::getText("record","s")->setPosition(1250,220);
    
    
    ReadWrite<Score> rw("score.sav");
    std::vector<Score> vec = rw.read();
        
    /*bool big = false;
    
    std::cout << "1s: " << vec[vec.size()].score << " s2: " << s << std::endl;
    if(vec[vec.size()].score == s){
        if(vec[vec.size()].score2 > 99) vec[vec.size()].score2 = 0;
    std::cout << "2s: " << vec[vec.size()].score2 << " s2: " << s2 << std::endl;
        if(vec[vec.size()].score2 < s2){
            big = true;
            Console::log(true);
        }
    }else{
        if(vec[vec.size()].score < s){
            big = true;
        }
    }*/
    
    float lastSco = 0;
    
    if(vec.size() != 0){
        lastSco = vec[vec.size()-1].score+(vec[vec.size()-1].score2*0.01);    
    }
    
    
    float neSco = (s+(s2*0.01));
    //std::cout << "aas: " << lastSco << std::endl;
    //std::cout << "aas2: " << neSco << std::endl;
    
    if(vec.size() < 24 || neSco > lastSco){
                
        char c[1];
        c[0] = '_';
        
        Score nue(c,s,s2);
            
        vec.push_back(nue);
        std::sort(vec.begin(), vec.end());
        std::reverse(vec.begin(), vec.end());
        
        
        for(unsigned int i=0; i<vec.size(); i++){
            if(vec[i].name[0] == '_'){ 
                _nPos = (int)i;
                vec[i].name[0] = ' ';
                break;
            }
        }

        if(vec.size()>24){        
            vec.erase(vec.begin()+24, vec.end());
        }
    }
    
    String name;
    String score;
    float sco;
    String num;
    
    for(unsigned int i=0; i<vec.size(); i++){
        num = StringConverter::toString(i+1);
        
        name = vec[i].name;
        sco = vec[i].score+(vec[i].score2*0.01);
        score = StringConverter::toString(sco);
        
        GUIContainer::createText("record","pos"+num, num+":");
        GUIContainer::getText("record","pos"+num)->setCharHeight(0.03);
        GUIContainer::getText("record","pos"+num)->setPosition(500,220+(i+1)*20);
        
        GUIContainer::createText("record","n"+num, name);
        GUIContainer::getText("record","n"+num)->setText(name);
        GUIContainer::getText("record","n"+num)->setCharHeight(0.03);
        GUIContainer::getText("record","n"+num)->setPosition(540,220+(i+1)*20);
        
        GUIContainer::createText("record","l"+num, "_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _");
        GUIContainer::getText("record","l"+num)->setColor(ColourValue(0.7,0.7,0.7));
        GUIContainer::getText("record","l"+num)->setCharHeight(0.03);
        GUIContainer::getText("record","l"+num)->setPosition(500,225+(i+1)*20);
                
        GUIContainer::createText("record","s"+num, score);
        GUIContainer::getText("record","s"+num)->setText(score);
        GUIContainer::getText("record","s"+num)->setAlignment(TextAreaOverlayElement::Right);
        GUIContainer::getText("record","s"+num)->setCharHeight(0.03);
        GUIContainer::getText("record","s"+num)->setPosition(1250,220+(i+1)*20);
        
        if((int)i == _nPos){
            GUIContainer::getText("record","pos"+num)->setColor(ColourValue::Green);
            GUIContainer::getText("record","n"+num)->setColor(ColourValue::Green);
            GUIContainer::getText("record","s"+num)->setColor(ColourValue::Green);
            
            GUIContainer::getText("record","n"+num)->setText("");
            GUIContainer::getText("record","s"+num)->setText(_score);
        }
    }
    
    _vec = vec;
}

void OverState::submitScore() {
    ReadWrite<Score> rw("score.sav");
        
    String num = StringConverter::toString(_nPos+1);
    
    
    for(unsigned int i=0; i<20; i++){
        
        _vec[_nPos].name[i] = GUIContainer::getText("record","n"+num)->getText()[i];
    }
    
    
    rw.write(_vec, true);
    
    exitToMenu();
}

void OverState::exitToMenu() {
        
    if(_nPos != -1){

        GUIContainer::getTextField("over","textf")->getText()->setText(" ");

        String num = StringConverter::toString(_nPos+1);

        GUIContainer::getText("record","pos"+num)->setColor(ColourValue::White);
        GUIContainer::getText("record","n"+num)->setColor(ColourValue::White);
        GUIContainer::getText("record","s"+num)->setColor(ColourValue::White);

    }
    GUIContainer::getGroup("over")->setAllvisible(false);
        
    popState();
    PlayState::getSingletonPtr()->exitState();
}

