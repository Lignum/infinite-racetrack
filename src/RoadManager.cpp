#include "Object/Infinite/RoadManager.h"
#include "GUI/Console.h"
#include "GUI/GUIContainer.h"
#include "Object/Infinite/CheckPoint.h"

int RoadManager::_maxCar;

RoadManager::RoadManager(GameObjectManager* gom) {
    _gom = gom;
    
    _position = Vector3(0,0,0);
    
    _car = new CarControler(_gom, _position + Vector3(12,0,0));
    
    _roadsNum = 0;
    
    GUIContainer::createText("play","KM","KM");
    GUIContainer::getText("play","KM")->setAlignment(TextAreaOverlayElement::Right);
    GUIContainer::getText("play","KM")->setPosition(300,700);
    GUIContainer::getText("play","KM")->setCharHeight(0.06);
    
    createFirst();
}

void RoadManager::reset() {
    for(unsigned int i=0; i<_roads.size(); i++){
        _gom->destroyObject(_roads[i]->getName());
    }
    
    _roads.clear();
    _roadsNum = 0;
    
    createFirst();
}

void RoadManager::createFirst() {
    _roads.push_back(new Road(_gom, Road::MESH_S1, _position, Road::DIR_N));
    CheckPoint::initTime();
    CheckPoint::initCar(_car);
    
    _lastR = _roads[0];
    
    _left = 0;
    _lastRoad = 0;
    _lastRoadKm = 0;
    _km = 0;
    _roadsNum = 0;
    _nextRoadCar = 0;
    _kmCheck = 0;
    _floorNum = 0;
    _maxCar = 20;
    
    _upDown = 0;
    
    /*_roadsTypeRand.push_back(100);
    _roadsTypeRand.push_back(0);
    _roadsTypeRand.push_back(0);
    _roadsTypeRand.push_back(0);
    
    
    _roadsTypeAument.push_back(5);
    _roadsTypeAument.push_back(0);
    _roadsTypeAument.push_back(0);
    _roadsTypeAument.push_back(0);*/

    _roadsTypeRand.push_back(100);
    _roadsTypeRand.push_back(40);
    _roadsTypeRand.push_back(4);
    _roadsTypeRand.push_back(0);
    
    
    _roadsTypeAument.push_back(5);
    _roadsTypeAument.push_back(1);
    _roadsTypeAument.push_back(0.5);
    _roadsTypeAument.push_back(0.2);
    
    //int i = -1;
    //int j = -1;
    
    
    /*while(j<=1){
        
        _floors.push_back(new Floor(_gom, Vector3(i,-1,j)));
        
        if(i>=1){
            i = -1;
            j++;
        }else{
            i++;
        }
    }*/
    
    Car::setRoads(&_roads);
    Car::setCarControler(_car);

    
    _roadsNum += 1;
}

void RoadManager::createNext() {
    
    //std::cout << "create next init" << std::endl;    
    createRoad();
    
    //std::cout << "create floors" << std::endl;
    //createFloors();
    
    //std::cout << "calculate km" << std::endl;
    calculateKm();
    
    //std::cout << "create car" << std::endl;
    createCar();
    
    //std::cout << "create next end" << std::endl;
}

void RoadManager::createRoad() {
    if(_roadsNum < _maxRoads){

        
        if(_lastR->distance(_car)<_roadsDistance){

            int ran;
            int last = 0;
            int max = 0;

            for(unsigned int i=0; i<_roadsTypeRand.size(); i++){
                if((int)_roadsTypeRand[i] != 0){
                    ran = rand()% (int)_roadsTypeRand[i];
                    if(ran>last){
                        last = ran;
                        max = i;
                    }
                }
            }

            _roadsTypeRand[max] -= _roadsTypeAument[max]*2.5;

            for(unsigned int i=0; i<_roadsTypeRand.size(); i++){
                _roadsTypeRand[i] += _roadsTypeAument[i];
                if(_roadsTypeRand[i]>=100)_roadsTypeRand[i]=100;
            }

            bool check = false;
            
            if(_kmCheck >= _kmTotalCheck){
                check = true;
                _kmCheck = 0;
                max = 0;
            }

            bool dir = true;
            if(max != 0){
                if(rand() % 2 > _left){
                    dir = false;
                    _left++;
                }else{
                    _left--;
                }
            }
            
            Console::setStatic("total", "total", _totalRoads);
            Console::setStatic("s", "str", _roadsTypeRand[0]);
            Console::setStatic("3", "3", _roadsTypeRand[1]);
            Console::setStatic("2", "2", _roadsTypeRand[2]);
            Console::setStatic("1", "1", _roadsTypeRand[3]);
            Console::setStatic("check", "kmCheck", _kmCheck);
            //dir =5;
            

            Road* r;
            
            int rrr = rand()% 3;
            
            switch(max){
                case 0://RECTA
                    
                    rrr -= 1;
                    _upDown += rrr;
                    
                    if(_upDown<-1) _upDown = -1;
                    if(_upDown>1) _upDown = 1;
                    
                    r = new Road(_gom, Road::MESH_S1, _lastR, check,_upDown);
                    break;

                case 1://CURVA R3
                    if(dir){
                        r = new Road(_gom, Road::MESH_R3, _lastR, check,0);
                    }else{//CURVA L3
                        r = new Road(_gom, Road::MESH_L3, _lastR, check,0);
                    }
                    break;

                case 2://CURVA R2
                    if(dir){
                        r = new Road(_gom, Road::MESH_R2, _lastR, check,0);
                    }else{//CURVA L2
                        r = new Road(_gom, Road::MESH_L2, _lastR, check,0);
                    }
                    break;

                case 3://CURVA R
                    if(dir){
                        r = new Road(_gom, Road::MESH_R1, _lastR, check,0);
                    }else{//CURVA L                
                        r = new Road(_gom, Road::MESH_L1, _lastR, check,0);
                    }
                    break;
            
            }
            
            if(r->getSize() == 0){
                _kmCheck += _kmPiece;
            }else{
                _kmCheck += (Math::PI*(2*fabs(r->getSize()*_kmPiece)/4));
            }
            
            
            if(_roads.size()<_maxRoads){
                _roads.push_back(r);
            }else{
                _roads[_lastRoad] = r;
                
                _lastRoad++;
                if(_lastRoad >= _maxRoads) _lastRoad = 0;
            }
                        
            _lastR = r;
            
            _roadsNum++;
            _totalRoads++;
            
        
        }
        
    }else{
        if(_roads[_lastRoad]->distance(_car)>_roadsDistance){
            if(_lastRoad>_maxRoads) _lastRoad = 0;
            _gom->destroyObject(_roads[_lastRoad]->getName());
            //_roads.erase(_roads.begin()+ _lastRoad);
            _roads[_lastRoad] = NULL;
            _roadsNum--;
        }
    }
}

void RoadManager::createFloors() {
    
    Vector2 pos = Vector2(_car->getPosition().x, _car->getPosition().z)/Floor::SIZE;
    
    //pos += Vector3(0,0,0.5);
    
    int i = pos.x-1;
    int j = pos.y-1;
        
    bool place = true;
    
    while(j<=pos.y+1){
        place = true;
        
        //Comprueba si ya hay un suelo en esa posicion
       for(unsigned int e=0; e<_floors.size(); e++){
            if(_floors[e]->getRelativePosition() == Vector3(i,-1,j)){
                place = false;
                break;
            }
        }
        
        if(place){
            
            //Comprueba que el suelo elegido no se el mismo que en el que esta el coche
        //std::cout << "aa: " << _floorNum << std::endl;
            if(_floorNum>8) _floorNum = 0;
            if(_floors[_floorNum]->distance(_car)<Floor::SIZE){
                _floorNum++;
                if(_floorNum>8) _floorNum = 0;
            }
            
            //elimina el suelo elegido y crea otro en su lugar
            _gom->destroyObject(_floors[_floorNum]->getName());
            _floors[_floorNum] = new Floor(_gom, Vector3(i,-1,j));
            _floorNum++;
            if(_floorNum>8) _floorNum = 0;
        }
        
        if(i>=pos.x+1){
            i = pos.x-1;
            j++;
        }else{
            i++;
        }
    }
}

void RoadManager::calculateKm() {
    
    if(_lastRoadKm > _maxRoads) _lastRoadKm = 0;
    
    if(_car){
        if(_roads[_lastRoadKm]->distance(_car) > Road::DISTANCE && 
            _roads[_lastRoadKm]->distance(_car) < Road::DISTANCE*2){
            if(_roads[_lastRoadKm]->getSize() == 0){
                _km += _kmPiece;
            }else{
                _km += (Math::PI*(2*fabs(_roads[_lastRoadKm]->getSize()*_kmPiece)/4));
            }

            _lastRoadKm++;
            if(_lastRoadKm > _roadsNum) _lastRoadKm = 0;
        }

        float k = (int)(_km*100);
        k = k/100;

        String s = Ogre::StringConverter::toString(k);
        GUIContainer::getText("play","KM")->setText(s+"\tKm");
    }
    
    
}

void RoadManager::createCar() {
    
    
    Console::setStatic("cars", "Cars", Car::getNumCars());
    
    if(Car::_numCars< _maxCar){

        int num = rand()% 6;

        float ran;

        switch(num){
            case 5: ran = -12.5;
                break;
            case 4: ran = -7.5;
                break;
            case 3: ran = -2.5;
                break;
            case 2: ran = 2.5;
                break;
            case 1: ran = 7.5;
                break;
            case 0: ran = 12.5;
                break;
        }
        //ran = -12.5;
        //ran = -7.5;
        //ran = -2.5;
        //ran = 2.5;
        //ran = 7.5;
        //ran = 12.5;
        
        if(_roads[_nextRoadCar] != NULL){
            
            Road* road = _roads[_nextRoadCar];

            if(road->getSize() == 0){
                
                if(road->getPosition().z < _car->getPosition().z){
                    
                    if(road->distance(_car) > _carCreateDistanceMin && road->distance(_car) < _carCreateDistanceMax){
                        switch(road->getDirection()){
                            case Road::DIR_N:

                                new Car(_gom, road->getPosition()+Vector3(ran,0,0), Vector2(0,-1), _nextRoadCar, (num+1)*8, ran);
                                break;
                            case Road::DIR_E:

                                new Car(_gom, road->getPosition()+Vector3(0,0,ran), Vector2(1,0), _nextRoadCar, (num+1)*8, ran);
                                break;
                            case Road::DIR_S:

                                new Car(_gom, road->getPosition()+Vector3(-ran,0,0), Vector2(0,1), _nextRoadCar, (num+1)*8, ran);
                                break;
                            case Road::DIR_W:

                                new Car(_gom, road->getPosition()+Vector3(0,0,-ran), Vector2(-1,0), _nextRoadCar, (num+1)*8, ran);
                                break;
                        }
                    }                    
                }
            }
        }
        
        _nextRoadCar++;
        if(_nextRoadCar >= (int)_roads.size()){
            _nextRoadCar = 0;
        }
    }
    
}

int RoadManager::getRoadsNum() {
    return _roadsNum;
}

CarControler* RoadManager::getCar() {
    return _car;
}

void RoadManager::addMaxCars(int num) {
    _maxCar += num;
}

int RoadManager::getMaxRoads() {
    return _maxRoads;
}

float RoadManager::getKms() {
    return _km;
}
