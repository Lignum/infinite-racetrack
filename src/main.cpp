#define UNUSED_VARIABLE(x) (void)x

#include "States/GameManager.h"
#include "States/IntroState.h"
#include "States/PlayState.h"
#include "States/PauseState.h"
#include "States/MenuState.h"
#include "States/OverState.h"

#include <iostream>

#include "GUI/GUI.h"


#include "File/ReadWrite.h"
#include "File/Score.h"

using namespace std;

int main () {
    
    
    /*ReadWrite<Score> rw("score.sav");
    
    char c[20];
    for(unsigned int i=0; i<20; i++){
        //c[i] = GUIContainer::getTextField("over", "field")->getText()->getText()[i];
        c[i] = 'a';
    }
    
    Score s(c,100,2);
    
    std::vector<Score> list = rw.read();
    list.push_back(s);
    std::sort(list.begin(), list.end());
    std::reverse(list.begin(), list.end());
    
    if(list.size()>24){        
        list.erase(list.begin()+24, list.end());
    }
    
    rw.write(list, true);
    */

  GameManager* game = new GameManager();
  IntroState* introState = new IntroState();
  MenuState* menuState = new MenuState();
  PlayState* playState = new PlayState();
  PauseState* pauseState = new PauseState();
  OverState* overState = new OverState();

  UNUSED_VARIABLE(introState);
  UNUSED_VARIABLE(menuState);
  UNUSED_VARIABLE(playState);
  UNUSED_VARIABLE(pauseState);
  UNUSED_VARIABLE(overState);
    
  try
    {
      // Inicializa el juego y transición al primer estado.
      game->start(IntroState::getSingletonPtr());
    }
  catch (Ogre::Exception& e)
    {
      std::cerr << "Excepción detectada: " << e.getFullDescription();
    }
  
  delete game;
  
  return 0;
}
