#include <string>

#include "Object/GameObject.h"
#include "Object/GameObjectManager.h"
#include "GUI/Console.h"

GameObject::GameObject(GameObjectManager* gom, const String& mesh){
    init(gom->getSceneManager(), mesh, Vector3(0, 0, 0));
    gom->addObject(this);
}

GameObject::GameObject(GameObjectManager* gom, const String& mesh, Vector3 pos){
    init(gom->getSceneManager(), mesh, pos);
    gom->addObject(this);
}

GameObject::GameObject(GameObjectManager* gom, const String& name, const String& mesh){
    init(gom->getSceneManager(), name, mesh, Vector3(0, 0, 0));
    gom->addObject(this);
}

GameObject::GameObject(GameObjectManager* gom, const String& name, const String& mesh, Vector3 pos){
    init(gom->getSceneManager(), name, mesh, pos);
    gom->addObject(this);
}

GameObject::~GameObject() {
    
    _node->detachAllObjects();
    
}


void GameObject::init(SceneManager* sce, const String& mesh, Vector3 pos) {
    _sceneMgr = sce;    
    _node = sce->createSceneNode();
    Ogre::Entity* ent = sce->createEntity(mesh);
    ent->setCastShadows(true);
    _node->attachObject(ent);
    _node->setPosition(pos);
    sce->getRootSceneNode()->addChild(_node);
    
    _color = Vector3(1,1,1);
    
    tag = "none";
}

void GameObject::init(SceneManager* sce, const String& name, const String& mesh, Vector3 pos){
    _sceneMgr = sce;    
    _node = sce->createSceneNode(name);
    Ogre::Entity* ent = sce->createEntity(mesh);
    ent->setCastShadows(true);
    _node->attachObject(ent);
    _node->setPosition(pos);
    sce->getRootSceneNode()->addChild(_node);
    
    _color = Vector3(1,1,1);
        
    tag = "none";
}

void GameObject::update(Real delta) {
}

void GameObject::keyPressed(const OIS::KeyEvent& e) {

}

void GameObject::keyReleased(const OIS::KeyEvent& e) {

}

void GameObject::buttonPressed(const OIS::JoyStickEvent& e, int button) {

}

void GameObject::buttonReleased(const OIS::JoyStickEvent& e, int button) {

}

void GameObject::povMoved(const OIS::JoyStickEvent& e, int index) {

}

void GameObject::axisMoved(Real rel, int axis) {

}

void GameObject::setPosition(Vector3 vec){
    _node->setPosition(vec);
}

void GameObject::setScale(Vector3 vec) {
    _node->setScale(vec);
}
void GameObject::setScale(float scale){
    setScale(Vector3(scale, scale, scale));
}
void GameObject::translate(Real x, Real y, Real z){
    _node->translate(x,y,z);
}

void GameObject::translate(Vector3 vec) {
    _node->translate(vec);
}

void GameObject::lookAt(Vector3 vec) {
    _node->lookAt(vec, SceneNode::TS_WORLD);
}

void GameObject::rotX(Real degree) {
    _node->pitch(Ogre::Degree(degree));
}

void GameObject::rotY(Real degree) {
    _node->yaw(Ogre::Degree(degree));
}

void GameObject::rotZ(Real degree) {
    _node->roll(Ogre::Degree(degree));
}

Real GameObject::distance(GameObject* g) {
    return _node->getPosition().distance(g->_node->getPosition());
}

Real GameObject::distance(Vector3 point) {
    return _node->getPosition().distance(point);
}


void GameObject::setColor(Real r, Real g, Real b, Real a) {
    
    _color = Vector3(r,g,b);
    
    MaterialPtr mat = MaterialManager::getSingleton().create(static_cast<Ogre::Entity*>(
        _node->getAttachedObject(0))->
        getSubEntity(0)->getMaterialName()
            ,ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);    
    
    mat.getPointer()->getTechnique(0)->getPass(0)->createTextureUnitState(static_cast<Ogre::Entity*>(
        _node->getAttachedObject(0))->
        getSubEntity(0)->getMaterial().getPointer()->
        getTechnique(0)->getPass(0)->getTextureUnitState(0)->getTextureName());
    
    mat.getPointer()->setAmbient(r,g,b);
    mat.getPointer()->setDiffuse(r,g,b,a);
    static_cast<Ogre::Entity*>(_node->getAttachedObject(0))->getSubEntity(0)->setMaterial(mat);
}

void GameObject::setTexture(String texture) {
    
    MaterialPtr mat = MaterialManager::getSingleton().create(static_cast<Ogre::Entity*>(
        _node->getAttachedObject(0))->
        getSubEntity(0)->getMaterialName()
            ,ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);    
    
    mat.getPointer()->getTechnique(0)->getPass(0)->createTextureUnitState(texture);
    
    
    static_cast<Ogre::Entity*>(_node->getAttachedObject(0))->getSubEntity(0)->setMaterial(mat);
    
}

void GameObject::setVisible(bool visible) {
    _node->setVisible(visible, true);
}


void GameObject::addChild(GameObject* go){    
    
    _sceneMgr->getRootSceneNode()->removeChild(go->getNode());
    _node->addChild(go->getNode());
    
    _childs.push_back(go);
}

GameObject* GameObject::getChild(const String& name) {
    for(long unsigned int i = 0; i<_childs.size(); i++){
        if(_childs[i]->getName() == name){
            return _childs[i];
        }
    }
    
    return NULL;
}

GameObject* GameObject::getChild(int num){
    return _childs[num];
}

std::vector<GameObject*> GameObject::getChilds() {
    return _childs;
}


bool GameObject::collide(GameObject* object) {
    if(getWorldBoundingBox().intersects(object->getWorldBoundingBox())){
        return true;
    }
    return false;
}

bool GameObject::sphereCollide(GameObject* object) {
    if(getWorldBoundingSfere().intersects(object->getWorldBoundingBox())){
        
        return true;
    }
    return false;
}


AxisAlignedBox GameObject::collision(GameObject* object) {
    return getWorldBoundingBox().intersection(object->getWorldBoundingBox());
}


const Vector3& GameObject::getPosition(){
    return _node->getPosition();
}

const Vector3& GameObject::getScale() {
    return _node->getScale();
}

const Vector3& GameObject::getColor() {
    return _color;
}



const String& GameObject::getName() {
    return _node->getName();
}

void GameObject::setCastShadows(bool shadow) {
    static_cast<Ogre::Entity*>(_node->getAttachedObject(0))->setCastShadows(shadow);
}


Vector3 GameObject::getSize() {
    return getBoundingBox().getSize();
}


AxisAlignedBox GameObject::getWorldBoundingBox() {
    return static_cast<Entity*>(_node->getAttachedObject(0))->getWorldBoundingBox();
}

Sphere GameObject::getWorldBoundingSfere() {
    return static_cast<Entity*>(_node->getAttachedObject(0))->getWorldBoundingSphere();
}

AxisAlignedBox GameObject::getBoundingBox() {
    return static_cast<Entity*>(_node->getAttachedObject(0))->getBoundingBox();
}


SceneNode* GameObject::getNode(){
    return _node;
}

SceneManager* GameObject::getSceneManager() {
    return _sceneMgr;
}

