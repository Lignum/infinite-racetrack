#include "Object/Infinite/Floor.h"

Floor::Floor(GameObjectManager* gom, Vector3 pos) : PhysicGameObject(gom, true, "Plane.mesh", pos*Vector3(SIZE,HEIGHT,SIZE)) {
    tag = "Floor";
}

Vector3 Floor::getRelativePosition() {
    return PhysicGameObject::getPosition()/Vector3(SIZE,HEIGHT,SIZE);
}
