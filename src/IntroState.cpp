#include "States/IntroState.h"

#include "States/MenuState.h"

template<> IntroState* Ogre::Singleton<IntroState>::msSingleton = 0;

IntroState::IntroState() {
    
}

void IntroState::enter (){
    _root = Ogre::Root::getSingletonPtr();

    _sceneMgr = _root->createSceneManager(Ogre::ST_GENERIC, "SceneManager");
    _camera = _sceneMgr->createCamera("IntroCamera");
    _viewport = _root->getAutoCreatedWindow()->addViewport(_camera);
    
    _viewport->setBackgroundColour(ColourValue(0.5, 0.6, 0.9));

    initSDL(); //Inicia la biblioteca de sonido
    
    //_viewport->setBackgroundColour(Ogre::ColourValue(0.5, 0.0, 0.0));
    
    _camera->setPosition(0,0,0);
    //_camera->setDirection(0,0,1);
    
    _camera->setNearClipDistance(0.1f); // 
    _camera->setFarClipDistance(100.0f);
    _camera->setAspectRatio(Ogre::Real(_viewport->getActualWidth()) / Ogre::Real(_viewport->getActualHeight()));
    
    //
    
    
    _exitGame = false;
    
    GUIContainer::init(_viewport);
    //GUIContainer::createCursor("cursor.png");
    //GUIContainer::showCursor(false);
    GUIContainer::setDefaultTextFont("Racer");
    GUIContainer::setDefaultTextCharHeight(0.06);
    GUIContainer::setDefaultTextColor(ColourValue(1,1,1));
    
    _pTrackManager = new TrackManager;
    _pSoundFXManager = new SoundFXManager;
    
    /*
     * #########################################################################
     * DEBUG
     * #########################################################################
     */
    Console::init(_viewport);
    Console::setVisible(false);
    _sceneMgr->showBoundingBoxes(false);
    
}


void IntroState::exit(){
    //_sceneMgr->clearScene();
    //_root->getAutoCreatedWindow()->removeAllViewports();
}

void IntroState::pause (){
}

void IntroState::resume (){
    
    
}

bool IntroState::frameStarted(const Ogre::FrameEvent& evt){
    Console::setStatic("FPS", "FPS" ,1.0 / evt.timeSinceLastFrame);
    return true;
}

bool IntroState::frameEnded(const Ogre::FrameEvent& evt){
    
    
    changeState(MenuState::getSingletonPtr());
    
    if (_exitGame)
        return false;
    
    return true;
}

void IntroState::keyPressed(const OIS::KeyEvent &e){
    
}

void IntroState::keyReleased(const OIS::KeyEvent &e ){
    if (e.key == OIS::KC_BACK) {
        _exitGame = true;
    }
}

void IntroState::buttonPressed(const OIS::JoyStickEvent& e, int button) {

}

void IntroState::buttonReleased(const OIS::JoyStickEvent& e, int button) {

}

void IntroState::axisMoved(const OIS::JoyStickEvent& e, int axis) {

}

void IntroState::sliderMoved(const OIS::JoyStickEvent& e, int index) {

}

void IntroState::povMoved(const OIS::JoyStickEvent& e, int index) {

}

void IntroState::vector3Moved(const OIS::JoyStickEvent& e, int index) {

}

void IntroState::mouseMoved(const OIS::MouseEvent &e){
   
    
}

void IntroState::mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id){
    
}

void IntroState::mouseReleased(const OIS::MouseEvent &e, OIS::MouseButtonID id){
    
}

IntroState* IntroState::getSingletonPtr (){
    return msSingleton;
}

IntroState& IntroState::getSingleton (){ 
    assert(msSingleton);
    return *msSingleton;
}

bool IntroState::initSDL () {
    // Inicializando SDL...
    if (SDL_Init(SDL_INIT_AUDIO) < 0)
        return false;
    // Llamar a  SDL_Quit al terminar.
    atexit(SDL_Quit);
 
    // Inicializando SDL mixer...
    if (Mix_OpenAudio(MIX_DEFAULT_FREQUENCY, MIX_DEFAULT_FORMAT,MIX_DEFAULT_CHANNELS, 4096) < 0)
      return false;
 
    // Llamar a Mix_CloseAudio al terminar.
    atexit(Mix_CloseAudio);
 
    return true;    
}