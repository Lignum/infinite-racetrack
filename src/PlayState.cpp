#include "States/PlayState.h"

#include "States/MenuState.h"
#include "States/PauseState.h"
#include "States/IntroState.h"
#include "Object/Infinite/Car.h"
#include "Object/Infinite/CheckPoint.h"
#include "States/OverState.h"

template<> PlayState* Ogre::Singleton<PlayState>::msSingleton = 0;

PlayState::PlayState() {

}


void PlayState::enter (){
    
    
    _root = Ogre::Root::getSingletonPtr();
    // Se recupera el gestor de escena y la cámara.
    _sceneMgr = _root->getSceneManager("SceneManager");
    _camera = _sceneMgr->getCamera("IntroCamera");
    _viewport = _root->getAutoCreatedWindow()->addViewport(_camera);
    
    _camera->setPosition(0,0,0);
    // Nuevo background colour.
    //_viewport->setBackgroundColour(Ogre::ColourValue(1.0, 1.0, 1.0));
    _exitGame = false;
    
    Console::log("PLAY STATE -- ENTER");
    
    _sceneMgr->setSkyBox(true, "tropical",500,false);
    
    _mainTrack = TrackManager::getSingletonPtr()->load("RaceTrack.wav");
    _mainTrack->play();
    
    
    
    _simpleEffect = SoundFXManager::getSingletonPtr()->load("startBeeps.wav");
    _counter = 200;
    
    
    //_simpleEffect = SoundFXManager::getSingletonPtr()->load("big-squeak.wav");
    //_simpleEffect->setVolumen(10);
    //_simpleEffect->playChannel(-1, 10);
    
    
   /* 
     * #########################################################################
     * LUCES
     * #########################################################################
     */
    
    
    _sceneMgr->setShadowTechnique(Ogre::SHADOWTYPE_STENCIL_ADDITIVE);
    _sceneMgr->setShadowFarDistance(1000);
    
    _sceneMgr->setShadowColour(Ogre::ColourValue(0.5, 0.5, 0.5) );
    _sceneMgr->setAmbientLight(Ogre::ColourValue(0.5, 0.5, 0.5));
    
    Ogre::Light* light = _sceneMgr->createLight("Light1");
    light->setDiffuseColour(0.9, 0.9, 0.9);
    light->setType(Ogre::Light::LT_DIRECTIONAL);
    light->setDirection(Ogre::Vector3(-0.2,-0.5,0.5));
    light->setSpotlightInnerAngle(Ogre::Degree(25.0f));
    light->setSpotlightOuterAngle(Ogre::Degree(60.0f));
    light->setSpotlightFalloff(5.0f);
    light->setCastShadows(true);
    
    /* 
     * #########################################################################
     * OBJETOS
     * #########################################################################
     */
      
    
    createUI();

    _gom = new GameObjectManager(_sceneMgr);
    
    _roadMng = new RoadManager(_gom);
    _car = _roadMng->getCar();    
    
    _camCon = new CameraControl(_gom, _camera, _car);
    
    
    _generate = true;
    _camSpeed = 100;
    
    
    
    
}

void PlayState::createUI() {
    
    GUIContainer::createGroup("play");
    
    GUIContainer::createImage("play","velo","Velocimetro.png");
    GUIContainer::getGUIImage("play","velo")->setPosition(900, 468);
        
    GUIContainer::createImage("play","veloA","VelocimetroA.png");
    GUIContainer::getGUIImage("play","veloA")->setPosition(900, 468);
    
    
    GUIContainer::getGUIImage("play","veloA")->setRotation(20.5);
    
    
    //COUNTER LIGHTS 1
    GUIContainer::createImage("play","l1","LR1.png");    
    GUIContainer::createImage("play","l2","LR1.png");    
    GUIContainer::createImage("play","l3","LR1.png");
    
    GUIContainer::getGUIImage("play","l1")->setPosition(598, 200);
    GUIContainer::getGUIImage("play","l2")->setPosition(658, 200);
    GUIContainer::getGUIImage("play","l3")->setPosition(718, 200);    
    
    //--2
    GUIContainer::createImage("play","l12","LR2.png");    
    GUIContainer::createImage("play","l22","LR2.png");    
    GUIContainer::createImage("play","l32","LR2.png");
    
    GUIContainer::getGUIImage("play","l12")->setPosition(598, 200);
    GUIContainer::getGUIImage("play","l22")->setPosition(658, 200);
    GUIContainer::getGUIImage("play","l32")->setPosition(718, 200);    
    
    GUIContainer::getGUIImage("play","l12")->setVisible(false);
    GUIContainer::getGUIImage("play","l22")->setVisible(false);
    GUIContainer::getGUIImage("play","l32")->setVisible(false);
    
    //--3
    GUIContainer::createImage("play","l13","LG1.png");    
    GUIContainer::createImage("play","l23","LG1.png");    
    GUIContainer::createImage("play","l33","LG1.png");
    
    GUIContainer::getGUIImage("play","l13")->setPosition(598, 200);
    GUIContainer::getGUIImage("play","l23")->setPosition(658, 200);
    GUIContainer::getGUIImage("play","l33")->setPosition(718, 200);    
    
    GUIContainer::getGUIImage("play","l13")->setVisible(false);
    GUIContainer::getGUIImage("play","l23")->setVisible(false);
    GUIContainer::getGUIImage("play","l33")->setVisible(false);
    
    
    
    //CHECK POINTS
    GUIContainer::createText("play","timer","2000");
    GUIContainer::getText("play","timer")->setPosition(705, 40);
    GUIContainer::getText("play","timer")->setAlignment(TextAreaOverlayElement::Right);
    
    
    GUIContainer::createText("play","timer2",".20");
    GUIContainer::getText("play","timer2")->setCharHeight(0.03);
    GUIContainer::getText("play","timer2")->setPosition(710, 53);
    GUIContainer::getText("play","timer2")->setAlignment(TextAreaOverlayElement::Left);
}


void PlayState::exit (){
    _gom->clear();
    delete _gom;
    
    GUIContainer::getGroup("play")->setAllvisible(false);
    
    
    _sceneMgr->clearScene();
    //_root->getAutoCreatedWindow()->removeAllViewports();
}

void PlayState::pause(){
}

void PlayState::resume(){
    Console::log("PLAY STATE -- RESUME");
}

bool PlayState::frameStarted(const Ogre::FrameEvent& evt){
    
    
    _delta = (1.0 / evt.timeSinceLastFrame);
    Console::setStatic("FPS", "FPS" ,(int)_delta);
    Console::setStatic("roads", "Roads", _roadMng->getRoadsNum());
    Console::setStatic("counter", "Count", _counter);
    
    
    //if(_delta > 50) Console::log(_delta);
    //if(_delta < 40) Console::err(_delta);
    //else if(_delta < 50) Console::war(_delta);
    
    
    if(_counter == 200) {
        CheckPoint::timer(evt.timeSinceLastFrame);
        _gom->update(evt.timeSinceLastFrame);
        _gom->update(evt.timeSinceLastFrame);
        _simpleEffect->playChannel(0, 30);
    }
    
    if(_counter>=0){
        counterLights();
        GUIContainer::update(evt.timeSinceLastFrame);
    }
    
        
    _counter =  _counter - 1*evt.timeSinceLastFrame;
        
    if(_counter <= 20){
        CheckPoint::timer(evt.timeSinceLastFrame);
        _gom->update(evt.timeSinceLastFrame);
    }
    
            
        //std::cout << "----------------------" << std::endl;
    _roadMng->createNext();
    
    if(_generate){
        _camCon->update(evt.timeSinceLastFrame);
    }else{        
        Vector3 camPos = _camera->getPosition();
        _camera->setPosition(camPos+Vector3(_camSpeed*_axis2*evt.timeSinceLastFrame, _camSpeed*_povU*evt.timeSinceLastFrame, _camSpeed*_axis3*evt.timeSinceLastFrame));
    }
    
    if(CheckPoint::timeOut()) gameOver();
    return true;
}

bool PlayState::frameEnded(const Ogre::FrameEvent& evt){
    
    
    if (_exitGame)
        return false;
    
    return true;
}


void PlayState::keyPressed(const OIS::KeyEvent &e){
    
    _gom->keyPressed(e);
    
      
    
    if(e.key == OIS::KC_ESCAPE){
        exitState();
    }
    
    /*
     * #########################################################################
     * DEBUG
     * #########################################################################
     */    
    if (e.text == 186) {
        bool visible = !Console::isVisible();
        Console::setVisible(visible);
        _sceneMgr->showBoundingBoxes(!_sceneMgr->getShowBoundingBoxes());
        //_gom->getWorld()->setShowDebugContactPoints(visible);
        _gom->getWorld()->setShowDebugShapes(visible);
    }
    
    if (e.key == OIS::KC_Q) {
        Console::setVisible(!Console::isVisible());
        
    }
    
    if(e.key == OIS::KC_O){
        _camera->setPosition(0,10,0.1);
        _camera->lookAt(0,-10,0);
        //_roadMng->createNext();
        _generate = !_generate;
    }  
}

void PlayState::keyReleased(const OIS::KeyEvent &e){
    
    _gom->keyReleased(e);
    
    /*
     * #########################################################################
     * DEBUG
     * #########################################################################
     */
    if (e.key == OIS::KC_0) {
        _exitGame = true;
    }
   
}

void PlayState::buttonPressed(const OIS::JoyStickEvent& e, int button) {
    _gom->buttonPressed(e, button);
    
    if(_counter <= 0){
        if(button == 9){
            pauseState();
        }
    }
    
    
    /*if(button == 8){
        gameOver();
    }*/
}

void PlayState::buttonReleased(const OIS::JoyStickEvent& e, int button) {
    _gom->buttonReleased(e, button);
}

void PlayState::axisMoved(const OIS::JoyStickEvent& e, int axis) {
    Real ax = e.state.mAxes[axis].abs;
    ax /= 32768;
    ax = (int)(ax*10000);
    ax /= 10000;
    if(ax>0){        
        ax += 0.0001;
    }
    if(ax>1) ax=1;
    if(ax<-1) ax=-1;
    
    if(ax<0.02 && ax>-0.02) ax=0;
    
    _gom->axisMoved(ax, axis);
    
    switch(axis){
        case 0:
            _axis0 = ax;
            break;
        case 1:
            _axis1 = ax;
            break;
        case 2:
            _axis2 = ax;
            break;
        case 3:
            _axis3 = ax;
            break;
            
    }
}

void PlayState::sliderMoved(const OIS::JoyStickEvent& e, int index) {

}

void PlayState::povMoved(const OIS::JoyStickEvent& e, int index) {
    _gom->povMoved(e, index);
    
    _povU = 0;
    
    if(e.state.mPOV[index].direction == e.state.mPOV[index].Centered){
        _povU = 0;
    }else if(e.state.mPOV[index].direction == e.state.mPOV[index].North){
        _povU = 1;
    }else if(e.state.mPOV[index].direction == e.state.mPOV[index].South){
        _povU = -1;
    }
}

void PlayState::vector3Moved(const OIS::JoyStickEvent& e, int index) {

}

void PlayState::mouseMoved(const OIS::MouseEvent &e){
        
}

void PlayState::mousePressed(const OIS::MouseEvent &e, OIS::MouseButtonID id){
    
}

void PlayState::mouseReleased(const OIS::MouseEvent &e, OIS::MouseButtonID id){

}

PlayState* PlayState::getSingletonPtr (){
    return msSingleton;
}

PlayState& PlayState::getSingleton (){ 
    assert(msSingleton);
    return *msSingleton;
}

void PlayState::counterLights() {
    if(_counter < 180){
        GUIContainer::getGUIImage("play","l12")->setVisible(true);
    }
    if(_counter < 130){
        GUIContainer::getGUIImage("play","l22")->setVisible(true);
    }
    if(_counter < 80){
        GUIContainer::getGUIImage("play","l32")->setVisible(true);
    }
    if(_counter < 20){
        GUIContainer::getGUIImage("play","l13")->setVisible(true);
        GUIContainer::getGUIImage("play","l23")->setVisible(true);
        GUIContainer::getGUIImage("play","l33")->setVisible(true);
    }
    
    if(_counter <= 0){
        GUIContainer::getGUIImage("play","l1")->setVisible(false);
        GUIContainer::getGUIImage("play","l2")->setVisible(false);
        GUIContainer::getGUIImage("play","l3")->setVisible(false);
        
        GUIContainer::getGUIImage("play","l12")->setVisible(false);
        GUIContainer::getGUIImage("play","l22")->setVisible(false);
        GUIContainer::getGUIImage("play","l32")->setVisible(false);
        
        GUIContainer::getGUIImage("play","l13")->setVisible(false);
        GUIContainer::getGUIImage("play","l23")->setVisible(false);
        GUIContainer::getGUIImage("play","l33")->setVisible(false);
    }
}

void PlayState::pauseState() {

    pushState(PauseState::getSingletonPtr());
    //changeState(MenuState::getSingletonPtr());
}

void PlayState::addCar() {
    _roadMng->addMaxCars(5);
}

void PlayState::exitState() {
    _mainTrack->stop();
    changeState(MenuState::getSingletonPtr());
}

void PlayState::gameOver() {    
    _mainTrack->stop();
    _car->silence();
    OverState::getSingletonPtr()->setScore(_roadMng->getKms());
    pushState(OverState::getSingletonPtr());
}
