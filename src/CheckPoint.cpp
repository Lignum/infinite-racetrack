#include "Object/Infinite/CheckPoint.h"
#include "GUI/Console.h"
#include "GUI/GUIContainer.h"
#include "Sound/Track.h"
#include "States/PlayState.h"

float CheckPoint::TIME;
CarControler* CheckPoint::_car;
bool CheckPoint::_timeLowStart;
SoundFXPtr CheckPoint::_timeLow;

CheckPoint::CheckPoint(GameObjectManager* gom, Vector3 pos, float rot) : PhysicGameObject(gom, true, "checkPoint.mesh", pos, Vector3(0,rot,0)) {
    addChild(new GameObject(gom, "signal.mesh"));
    _check = false;
    
    
    _sound = SoundFXManager::getSingletonPtr()->load("silenceCutted.wav");
}

void CheckPoint::update(Real delta) {
    if(!_check){
        if(collide(_car)){
            TIME += _plusTime;
            _check = true;                        
    
            GUIContainer::getText("play","timer")->setColor(ColourValue::White);
            GUIContainer::getText("play","timer2")->setColor(ColourValue::White);
            
            _sound->playChannel(0,20);
            
            PlayState::getSingletonPtr()->addCar();
        }
    }
}

void CheckPoint::initTime() {
    TIME = _initTime + 0.02;
    
    _timeLowStart = false;
    
    GUIContainer::getText("play","timer")->setColor(ColourValue::White);
    GUIContainer::getText("play","timer2")->setColor(ColourValue::White);
}

void CheckPoint::initCar(CarControler* car) {
    _car = car;
}

void CheckPoint::timer(Real delta) {
    TIME -= 1*delta;    
    
    GUIContainer::getText("play","timer")->setText(StringConverter::toString((int)TIME));
    
    int t = (TIME - (int)TIME) * 100;
    String s;
    
    if(t<10){
        s = ".0"+StringConverter::toString(t);
    }else{
        s = "."+StringConverter::toString(t);
    }
    
    GUIContainer::getText("play","timer2")->setText(s);
    
    if(TIME <= 10){
        
        if(!_timeLowStart){
            startSound();
        }
    
        GUIContainer::getText("play","timer")->setColor(ColourValue::Red);
        GUIContainer::getText("play","timer2")->setColor(ColourValue::Red);
    }
    
}

void CheckPoint::startSound() {

    _timeLow = SoundFXManager::getSingletonPtr()->load("tek.aif");
    //_timeLow->setVolumen(6, 10000);
    _timeLow->playChannel(-1, 6);
}

bool CheckPoint::timeOut() {
    if(TIME <= 0){
        _timeLow->setVolumen(6,0);
        return true;
    }
    return false;
}
