#include "Object/Infinite/CameraControl.h"
#include "GUI/Console.h"

CameraControl::CameraControl(GameObjectManager* gom, Camera* camera, CarControler* car){
    _camera = camera;
    _car = car;
    
}

void CameraControl::update(Real delta) {
    
    translateCamera(delta);
    
    _camera->lookAt(_car->getPosition()+Vector3(0,1,0));
}

void CameraControl::translateCamera(Real delta) {
    
    Vector3 carPos = _car->getPosition();
    
    Vector3 carDir = _car->getLinearVelocity()*Vector3(1,0,1);
    carDir = carDir.normalisedCopy();
    
    if(_car->getKmh()<1){
        carDir = _car->getNode()->getOrientation() * Vector3(0,0,1);
    }
    
    carPos += Vector3(-1,0,-1)*DISTANCE*carDir;
    carPos += Vector3(0,1,0)*HEIGHT;
    
    _camera->setPosition(carPos);
    
    
     
}
